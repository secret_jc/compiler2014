package appetizer.semantics;

import java.util.*;

public class Record extends Type
{
	public String name;
	public Env env;

	public int size;	//total memory size
	
	public Record(String n, Env e, int s)
	{
		name = n;
		env = e;
		size = s;
	}
}
