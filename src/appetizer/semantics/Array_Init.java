package appetizer.semantics;

public final class Array_Init extends Type
{
	public int capacity;
	public Type elementType;

	public Array_Init(Type t,int c)
	{
		elementType = t;
		capacity = c;
	}
}
