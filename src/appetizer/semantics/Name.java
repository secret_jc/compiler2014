package appetizer.semantics;

public final class Name extends Type 
{
	public String name;
	public Env env;
	
	public Name(String n, Env e)
	{
		name = n;
		env = e;
	}
}
