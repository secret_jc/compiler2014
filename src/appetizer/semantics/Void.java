package appetizer.semantics;

public final class Void extends Type
{
	private static Void res;

	public static Void getInstance()
	{
		if(res == null) 
		{
			res = new Void();
			return res;
		}
		else return res;
	}
	
}
