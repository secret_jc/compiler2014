package appetizer.semantics;

public final class TypeEntry extends Entry
{
	public TypeEntry(Type t)
	{
		super(t, false);
	}
}
