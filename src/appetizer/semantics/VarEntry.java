package appetizer.semantics;

import appetizer.intermediate.*;
import appetizer.semantics.*;

public final class VarEntry extends Entry
{
	public Temp loc;	
	public FuncEntry addFunc, locFunc;	//for global variable, remember whether or not add and loc is OK in current function
	public Temp add;			//for global variable
	public Label label;			//for global variable

	public int offset;
	public FuncEntry function;	//remember which function this variable is declared in


	public VarEntry(Type t, int o, FuncEntry entry)
	{
		super(t, true);
		offset = o;
		function = entry;
	}
	public Temp getLoc()
	{
		if(loc==null) loc = new Temp(true);
		return loc;
	}
}
