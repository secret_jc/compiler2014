package appetizer.semantics;

public class Entry
{
	public Type type;
	public boolean isLvalue;

	public Entry(Type t, boolean is)
	{
		isLvalue = is;
		type = t;
	}
}
