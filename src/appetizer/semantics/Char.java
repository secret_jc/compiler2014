package appetizer.semantics;

public final class Char extends Type
{
	private static Char res;

	public static Char getInstance()
	{
		if(res == null) 
		{
			res = new Char();
			return res;
		}
		else return res;
	}

}
