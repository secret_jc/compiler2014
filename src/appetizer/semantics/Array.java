package appetizer.semantics;

public final class Array extends Type
{
	public int capacity;
	public Type elementType;

	public Array(Type t,int c)
	{
		elementType = t;
		capacity = c;
	}
}
