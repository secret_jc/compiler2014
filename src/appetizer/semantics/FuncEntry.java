package appetizer.semantics;

import java.util.LinkedList;

import appetizer.intermediate.*;

public final class FuncEntry extends Entry
{
	public Label label;	//entry point in code
	public int varSize;	//total size of all declared variables including incoming parameters
	public int paraSize;	//total size of all incoming parameters
	public int tempSize;	//total size of temp things need to be remembered
	public LinkedList<VarEntry> parameters = new LinkedList<VarEntry>();

	public FuncEntry(Type t)
	{
		super(t, false);
	}
	
	public void AddPara(VarEntry v) {
		parameters.add(v);
	}
}
