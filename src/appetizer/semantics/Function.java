package appetizer.semantics;

import java.util.LinkedList;
import appetizer.ast.*;

public final class Function extends Type 
{
	public LinkedList<Type> argumentTypes; 
	public Type returnType;
	public boolean special;
	public String name;

	public Function(String n, Type t, LinkedList<Type> a)
	{
		name = n;
		returnType = t;
		argumentTypes = a;
	}
	public Function(String n, Type t)
	{
		name = n;
		returnType = t;
	}
}
