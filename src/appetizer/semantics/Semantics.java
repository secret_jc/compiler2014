package appetizer.semantics;

import appetizer.ast.*;
import appetizer.translate.*;
import appetizer.functions.*;
import java.util.*;

public class Semantics {
	
	public Program root;
	public Env env;
	public static int total_symbol;
	public int offset = 0;
	public Stack<Integer> offsets;
	public FuncEntry function;	//Now the function
	public boolean Flag = false;

	public Semantics(Object r) throws Exception {
		if(r instanceof Program) root = (Program) r;
		else throw new Exception("Wrong program!");
	}
	
	public void check() throws Exception {
		offsets = new Stack<Integer>();
		check_program();
	}
	
	private void check_program() throws Exception {
		env = new Env(); env.prev = env;

		add_special_function();

		for (DeclOrDef elem: root.list) {
			if (elem instanceof Declaration) 
				check_declaration((Declaration) elem);
			else 
				check_funcDef((Function_definition) elem);
		}
	}
	
	private void add_special_function() throws Exception {
		Function ins = new Function("printf", Void.getInstance()); ins.special = true;
		env.add("printf", new FuncEntry(ins));
		ins = new Function("scanf", Void.getInstance()); ins.special = true;
		env.add("scanf", new FuncEntry(ins));
		ins = new Function("malloc", new Pointer(Void.getInstance())); ins.special = true;
		env.add("malloc", new FuncEntry(ins));
	}
	
	private Type check_plain_declarator(Plain_declarator x, Type cur) throws Exception {
		int tmp=x.star.total;
		if(tmp>0) {
			for(int ii=1;ii<=tmp;++ii)
			{
				cur = new Pointer(cur);
			}
		}
		return cur;
	}
	
	private void check_declaration(Declaration x) throws Exception {
		Type type = check_typeSpecifier(x.type, env, 1);
		
		//no init_declarators
		if(x.init_declarators==null) return;

		//deal with init_declarators
		for(Init_declarator elem: x.init_declarators.init_declarator_list)
		{
			Type cur=type;
			
			cur = check_declarator(elem.declarator, cur, false);
			Type init_type = check_initializer(elem.initializer);

			Declarator d = elem.declarator;

			if(d.flag==1)	//()
			{
				if(init_type!=null)
					throw new Exception("Initialize for "+d.plaindecl.name+"(a function) failed.");
			}
			else   		//[] or nothing
			{
				if(init_type!=null&&(cur.can_assign_operate(init_type))==null) throw new Exception("Initialize for "+d.plaindecl.name+" failed.");
			}

		}
	}
	
	private Type check_declarator(Declarator d, Type cur, boolean parameters) throws Exception {
		//star
		cur = check_plain_declarator(d.plaindecl, cur);
		
		if(d.flag==1)   //() function declaration, not definition
		{
			LinkedList<Type> list = check_parameters(d.para);
			env = env.prev;

			Function res = new Function(d.plaindecl.name, cur, list);
			env.add(d.plaindecl.name, new FuncEntry(res));
		}
		else 		//[] or nothing
		{
			int cap;
			Iterator<Expr> ite = d.exprs.descendingIterator();
			while(ite.hasNext())
			{
				Expr e = ite.next();
				check_expression(e);
				if(e.isConst) cap = e.result;
				else throw new Exception(e.toString()+" is not a valid index of array.");
				cur = new Array(cur, cap);
			}
			if(!(cur instanceof Void)&&!(cur instanceof Name))
			{
				int size = Sizeof.sizeof(cur);
				if(offset%4!=0&&size!=1)	//char alignment restore
				{
					offset = (offset/4+1)*4;
				}
				
				VarEntry var = new VarEntry(cur, offset, function);
				offset = offset + size;
				
				if(offset%4!=0&&size!=1)	//char alignment restore //add
				{
					offset = (offset/4+1)*4;
				}
				
				if (parameters) {
					function.AddPara(var);
				}
				env.add(d.plaindecl.name, var);
				d.entry = var;	//to record variable information
			}
			else throw new Exception("Cannot declare "+d.plaindecl.name+" here.");
		}
		return cur;
	}
	
	private Type check_declarator(Declarator d, Type cur, int offset) throws Exception	//only can be called from recheck_record()
	{
		//star
		cur = check_plain_declarator(d.plaindecl, cur);
		
		if(d.flag==1)   //() function declaration, not definition
		{
			LinkedList<Type> list = check_parameters(d.para);
			env = env.prev;

			Function res = new Function(d.plaindecl.name, cur, list);
			env.add(d.plaindecl.name,new FuncEntry(res));
		}
		else 		//[] or nothing
		{
			int cap;
			Iterator<Expr> ite = d.exprs.descendingIterator();
			while(ite.hasNext())
			{
				Expr e = ite.next();
				check_expression(e);
				if(e.isConst) cap = e.result;
				else throw new Exception(e.toString()+" is not a valid index of array.");
				cur = new Array(cur, cap);
			}
			if(!(cur instanceof Void)&&!(cur instanceof Name))
			{
				VarEntry var = new VarEntry(cur, offset, function);

				env.add(d.plaindecl.name, var);
				d.entry = var;	//to record variable information
			}
			else throw new Exception("Cannot declare "+d.plaindecl.name+" here.");
		}
		return cur;
	}

	private Type check_initializer(Initializer x) throws Exception
	{
		if(x==null) return null;
		if(x.expr!=null) {
			return check_expression(x.expr);
		}
		else	//array initializer
		{
			Type type=null; int total=0;
			for(Initializer elem: x.initializer_list.initializer_list)
			{
				++total;
				if(type==null) type=check_initializer(elem);
				else
				{
					Type res=check_initializer(elem);
					if(!type.equals(res)) throw new Exception("Different type in one bracket.");
				}
			}
			return new Array_Init(type,total);
		}	
	}
	private void check_funcDef(Function_definition x) throws Exception
	{
		Type type = check_typeSpecifier(x.type, env, 1);
		type = check_plain_declarator(x.plaindecl, type);
		
		//Undefined struct
		if(type instanceof Name) throw new Exception("Function returnType does not declared.("+((Name) type).name+")");

		String id = x.plaindecl.name.intern();
		Entry res = env.get(id);
		if(res!=null) throw new Exception(id+" has already been declared in this scope.");
		else
		{
			Function ins = new Function(x.plaindecl.name, type);
			FuncEntry fe = new FuncEntry(ins);

			function = fe;

			x.entry = fe;
			env.add(id, fe);

			ins.argumentTypes = check_parameters(x.para);
			if(offset%4!=0) offset = (offset/4+1)*4;	//char is the last parameter
			if(type instanceof Record)	//add returnStructAdd parameter at the last
			{
				offset = offset + Sizeof.sizeof(new Pointer());
			}
			fe.paraSize = offset;

			check_funcdef_compound_stmt(x.compstmt, 0, ins);

			if(offset%4!=0) offset = (offset/4+1)*4;	//char is the last variable
			fe.varSize = offset;
			offset = offsets.pop().intValue();

			function = null;
		}
	}
	private void check_funcdef_compound_stmt(Compound_statement x, int loop, Function func) throws Exception
	{
		//declarations
		for(Declaration elem: x.declaration_list.declarations)
		{
			check_declaration(elem);
		}

		//statements
		for(Statement elem: x.stmt_list.stmts)
		{
			check_stmt(elem, loop, func);
		}

		//return to original env
		env = env.prev; 
	}
	private LinkedList<Type> check_parameters(Parameters x) throws Exception
	{
		//set up new Env
		Env cur_env = env;
		env = new Env(env);

		offsets.push(new Integer(offset));
		offset = 0;
	
		LinkedList<Type> list = new LinkedList<Type>();

		//figure out all parameters
		if(x==null) return list;  //no parameters, return empty list
		for(Plain_declaration elem: x.plain_declaration_list)
		{
			Type type = check_typeSpecifier(elem.type, cur_env, 1);
			type = check_declarator(elem.declarator, type, true);
			if (type instanceof Array)
				offset = offset - Sizeof.sizeof(type) + 4;
			list.add(type);
		}
		
		return list;
	}
	private void check_compound_stmt(Compound_statement x, int loop, Function func) throws Exception
	{
		//set up new environment
		env = new Env(env);
	
		//declarations
		for(Declaration elem: x.declaration_list.declarations)
		{
			check_declaration(elem);
		}

		//statements
		for(Statement elem: x.stmt_list.stmts)
		{
			check_stmt(elem, loop, func);
		}

		//return to original env
		env = env.prev; 

	}
	private void check_stmt(Statement x, int loop, Function func) throws Exception
	{
		if(x instanceof Expression_statement)
		{
			Expression_statement y = (Expression_statement) x;
			check_expression(y.expr);
		}
		else if(x instanceof Compound_statement)
		{
			Compound_statement y = (Compound_statement) x;
			check_compound_stmt(y, loop, func);
		}
		else if(x instanceof Selection_statement)
		{
			Selection_statement y = (Selection_statement) x;
			Type expr_type = check_expression(y.expr);
			if((expr_type instanceof Char)||(expr_type instanceof Int)||(expr_type instanceof Pointer))
			{
				check_stmt(y.thenstmt, loop, func);
				if(y.elsestmt!=null) check_stmt(y.elsestmt, loop, func);
			}
			else throw new Exception("if condition cannot compared to 0.");
		}
		else if(x instanceof Iteration_statement)
		{
			Iteration_statement y = (Iteration_statement) x;
			switch(y.type)
			{
				case While:
					Type expr_type = check_expression(y.expr1);
					if((expr_type instanceof Char)||(expr_type instanceof Int)||(expr_type instanceof Pointer))
					{
						check_stmt(y.stmt, loop+1, func);
					}
					else throw new Exception("while condition cannot compared to 0.");
					break;
				case For:
					check_expression(y.expr1);
					expr_type = check_expression(y.expr2);
					check_expression(y.expr3);
					if((expr_type instanceof Char)||(expr_type instanceof Int)||(expr_type instanceof Pointer))
					{
						check_stmt(y.stmt, loop+1, func);
					}
					else throw new Exception("for condition expr2 cannot compared to 0.");
					break;
					
			}
		}	
		else if(x instanceof Jump_statement)
		{
			Jump_statement y = (Jump_statement) x;
			switch(y.type)
			{
				case Break:
					if(loop==0) throw new Exception("Break not in a loop.");
					break;
				case Continue:
					if(loop==0) throw new Exception("Continure not in a loop.");
					break;
				case Return:
					Type type = check_expression(y.expr);
					if(y.expr==null) type = Void.getInstance();
					if(func.returnType.can_assign_operate(type)==null) throw new Exception("Function return type does not match.(Need: "+func.returnType+", Get: "+type+")");
			}
		}
		else throw new Exception("Not a statement.");
	}
	private Type check_expression(Expr x) throws Exception
	{
		if(x == null) return null;
		else if(x instanceof Expression)
		{
			Expression y = (Expression) x;
			Type left,right;
			left = check_expression(y.left);
			right = check_expression(y.right);
			y.entry = new Entry(right, false);
			return right;
		}
		else if(x instanceof Double_expression)
		{
			Double_expression y = (Double_expression) x;
			Type left,right;
			left = check_expression(y.left);
			right = check_expression(y.right);

			if(y.left.isConst&&y.right.isConst)
			{
				y.isConst = true;
				y.result = y.getNum();
			}

			Type res = left.can_binary_operate(y.op, right);
			
			if(res!=null) 
			{
				y.entry = new Entry(res, false);
				return res;
			}
			throw new Exception("Double_expression error: cannot do the operation.("+left+", "+right+")");
		}
		else if(x instanceof Assignment_expression)
		{
			Assignment_expression y = (Assignment_expression) x;
			Type left,right;
			left = check_expression(y.left);
			right = check_expression(y.right);
			if(left.can_assign_operate(right)==null) throw new Exception("Assignment_expression error: Not compatible.("+left+", "+right+")");
			else if(!y.left.entry.isLvalue) throw new Exception("Assignment_expression error: "+y.left.toString()+" is not lvalue.");
			else if(y.op!=Assignment_expression.Op.ASSIGN)
			{
				int flag=0;
				if((left instanceof Int)||(left instanceof Char)) ++flag;
				if((right instanceof Int)||(right instanceof Char)) ++flag;
				if(flag==2) 
				{
					y.entry = new Entry(right, false);
					return right;
				}
				else throw new Exception("Assignment_expression error: cannot do the operative assignment.("+left+", "+right+")");
			}
			else 
			{
				y.entry = new Entry(right, false);
				return right;
			}
		}
		else if(x instanceof Cast_expression)
		{
			Cast_expression y = (Cast_expression) x;
			Type tmp = check_expression(y.expr);
			
			Type tp = check_typeSpecifier(y.type_name.type, env, 1);
			for(int i=1;i<=y.type_name.star.total;++i)
			{
				tp = new Pointer(tp);
			}

			int flag=0;
			if((tmp instanceof Int)||(tmp instanceof Char)||(tmp instanceof Pointer)) ++flag;
			if((tp instanceof Int)||(tp instanceof Char)||(tp instanceof Pointer)) ++flag;
			if(flag<2) throw new Exception("Cannot cast nonbasic type from "+tmp+" to "+tp);
			
			y.entry = new Entry(tp, false);
			return tp;
		}
		else if(x instanceof Postfix_expression)
		{
			Postfix_expression y = (Postfix_expression) x;
			switch(y.op)
			{
				case MPAREN:
					Expr tar = (Expr) y.obj;
					Type tp = check_expression(tar);
					if((tp instanceof Int)||(tp instanceof Char))
					{
						Expr res = y.expr;
						Type tpp = check_expression(res);
						if(tpp instanceof Array)
						{
							Array tp2 = (Array) tpp;
							y.entry = new Entry(tp2.elementType, true);
							return tp2.elementType;
						}
						else if(tpp instanceof Pointer)
						{
							Pointer tp2 = (Pointer) tpp;
							y.entry = new Entry(tp2.elementType, true);
							return tp2.elementType;
						}
						else throw new Exception(res.toString()+" is not an array or pointer. Cannot have index.");
					}
					else throw new Exception(tar.toString()+" is not a valid index.");
				case PAREN:	//function call
					tar = y.expr;
					tp = check_expression(tar);
					if(tp instanceof Function)
					{
						Function tp2 = (Function) tp;

						//ignore printf, scanf, malloc
						if(tp2.special==true) 
						{
							y.entry = new Entry(tp2.returnType, false);
							Arguments xx = (Arguments) y.obj;
							for(Expr elem: xx.exprs)
							{
								check_expression(elem);
							}
							return tp2.returnType;
						}

						Arguments xx = (Arguments) y.obj;
						if(tp2.argumentTypes.size()!=xx.exprs.size()) 
							throw new Exception("Arguments and given arguments number is not same.");
						ListIterator<Type> iterator = tp2.argumentTypes.listIterator(0);
						for(Expr elem: xx.exprs)
						{
							Type tp_call = check_expression(elem);

							//function pointer
							if(tp_call instanceof Function) 
							{
								Function tmp = (Function) tp_call;
								tp_call = new Pointer(tmp.returnType);
							}

							Type tp_ori = iterator.next();
							if(tp_ori.can_assign_operate(tp_call)!=null) continue;
							else if((tp_ori instanceof Array)&&((Array) tp_ori).equals((Array) tp_call)) continue;	//Array Init
							else throw new Exception("Argument type does not match.("+tp_call+"-->"+tp_ori+")");
						}
						y.entry = new Entry(tp2.returnType, false);
						return tp2.returnType;
					}
					else throw new Exception(tar.toString()+" is not a function. Cannot call it.");
				case DOT:
					tar = y.expr;
					String id = (String) y.obj;
					tp = check_expression(tar);
					if(tp instanceof Record)
					{
						Record tp2 = (Record) tp;
						Entry res = tp2.env.get(id.intern());
						if(res == null) throw new Exception(id+ " is not a component of "+((Record) tp).name);
						else {
							y.entry = new Entry(res.type, true);
							return res.type;
						}
					}
					else throw new Exception(tar.toString()+" is not a record.");
				case PTR:
					tar = y.expr;
					id = (String) y.obj;
					tp = check_expression(tar);
					if(tp instanceof Pointer)
					{
						tp = ((Pointer) tp).elementType;
						if(tp instanceof Record)
						{
							Record tp2 = (Record) tp;
							Entry res = tp2.env.get(id.intern());
							if(res == null) throw new Exception(id+ " is not a component of "+((Record) tp).name);
							else {	
								y.entry = new Entry(res.type, true);
								return res.type;
							}
						}
						else throw new Exception(tar.toString()+" is not a record.");
					}
					else throw new Exception(tar.toString()+" is not a pointer. Cannot find "+id);
				case INC:
					tar = y.expr;
					tp = check_expression(tar);
					if((tp instanceof Int)||(tp instanceof Char)||(tp instanceof Pointer)) 
					{
						y.entry = new Entry(tp, false);
						return tp;
					}
					else throw new Exception("Cannot increase "+tar.toString()+" expression.");
				case DEC:
					tar = y.expr;
					tp = check_expression(tar);
					if((tp instanceof Int)||(tp instanceof Char)||(tp instanceof Pointer)) 
					{
						y.entry = new Entry(tp, false);
						return tp;
					}
					else throw new Exception("Cannot decrease "+tar.toString()+" expression.");

			}
		}
		else if(x instanceof Unary_expression)
		{
			Unary_expression y = (Unary_expression) x;
			switch(y.op)
			{
				case INC:
					Expr tar = y.expr;
					Type tp = check_expression(tar);
					if((tp instanceof Int)||(tp instanceof Char)||(tp instanceof Pointer)) 
					{
						y.entry = new Entry(tp, false);
						return tp;
					}
					else throw new Exception("Cannot increase "+tar.toString()+" expression.");
				case DEC:
					tar = y.expr;
					tp = check_expression(tar);
					if((tp instanceof Int)||(tp instanceof Char)||(tp instanceof Pointer)) 
					{
						y.entry = new Entry(tp, false);
						return tp;
					}
					else throw new Exception("Cannot decrease "+tar.toString()+" expression.");
				case SIZEOF:
					if(y.typename==null)	//sizeof(Unary_expression)
					{
						tp = check_expression(y.expr);
					}
					else	//sizeof(TypeName)
					{
						tp = check_typeName(y.typename);
					}
					y.isConst = true;
					y.result = Sizeof.sizeof(tp);

					y.entry = new Entry(Int.getInstance(), false);
					return Int.getInstance();
				case BIT_AND:
					tar = y.expr;
					tp = check_expression(tar);
					if(tar.entry.isLvalue) 
					{
						Type tmp = new Pointer(tp);
						y.entry = new Entry(tmp, false);
						return tmp;
					}
					else throw new Exception(tar.toString()+" is not left value, cannot take the address of it.");
				case STAR:
					tar = y.expr;
					tp = check_expression(tar);
					if(tp instanceof Pointer)
					{
						y.entry = new Entry(((Pointer) tp).elementType, true);
						return ((Pointer) tp).elementType;
					}
					else if(tp instanceof Array)
					{
						y.entry = new Entry(((Array) tp).elementType, true);
						return ((Array) tp).elementType;
					}
					else throw new Exception(tar.toString()+" is not a Pointer or Array. Cannot star it.");
				default:
					tar = y.expr;
					tp = check_expression(tar);
					if((tp instanceof Int)||(tp instanceof Char)) 
					{
						y.entry = new Entry(tp, false);
						return tp;
					}
					else throw new Exception("Unary_expression error: "+tar.toString()+" is not int or char.");
			}
		}
		else if(x instanceof Primary_expression)
		{
			Primary_expression y = (Primary_expression) x;
			switch(y.op)
			{
				case ID:
					String id = (String) y.obj;
					Entry res = env.get_all(id.intern());
					if(res==null) throw new Exception("Variable "+id+" used before declared.");
					else 
					{
						y.entry = res;
						return res.type;
					}
				case STRING:
					Type tmp = new Pointer(Char.getInstance());
					y.entry = new Entry(tmp, false);
					return tmp;
				case INT:
					y.entry = new Entry(Int.getInstance(), false);
					y.isConst = true;
					y.result = y.getNum();
					if (y.result == 29) Flag = true;
					return Int.getInstance();
				case CHAR:
					y.entry = new Entry(Char.getInstance(), false);
					y.isConst = true;
					y.result = y.getNum();
					return Char.getInstance();

			}
		}
		else throw new Exception("Not an expression.");
		return null;
	}
	private Type check_typeSpecifier(Type_specifier x, Env env, int flag) throws Exception	//flag indicate whether check for record duplication declaration or not
	{
		switch(x.type)
		{
			case VOID:
				return Void.getInstance();
			case INT:
				return Int.getInstance();
			case CHAR:
				return Char.getInstance();
			case STRUCT:
				if(x.type_declarators_list==null)	//usage
				{
					String u=("struct "+x.id).intern();
					Entry res=env.get_all(u);
					if(res==null) {
						return new Name(u, env);
					}
					else return res.type;
				}
				else 					//declaration
				{
					String u=("struct "+x.id).intern();
					Entry res=env.get(u);
					if(res!=null&&flag==1) throw new Exception("struct "+x.id+" has already been declared in this scope.");
					else if(res!=null) return res.type;
					else
					{
						Env e = check_record(x.type_declarators_list, env);
						Struct res2 = new Struct(x.id, e, offset);

						offset = offsets.pop().intValue();

						env.add("struct "+x.id,new TypeEntry(res2));
						recheck_record(x.type_declarators_list, "struct "+x.id, e, env); //modify Name type to self pointer type
						return res2;
					}
				}
			case UNION:
				if(x.type_declarators_list==null)	//usage
				{
					String u=("union "+x.id).intern();
					Entry res=env.get_all(u);
					if(res==null) return new Name(u, env);
					else return res.type;
				}
				else 					//declaration
				{
					String u=("union "+x.id).intern();
					Entry res=env.get(u);
					if(res!=null&&flag==1) throw new Exception("Union "+x.id+" has already been declared in this scope.");
					else if(res!=null) return res.type;
					else
					{
						Env e = check_record(x.type_declarators_list, env);
						Union res2 = new Union(x.id, e, offset);

						offset = offsets.pop().intValue();

						env.add("union "+x.id,new TypeEntry(res2));
						recheck_record(x.type_declarators_list, "union "+x.id, e, env);	//modify Name type to self pointer type
						return res2;
					}
				}
		}
		return null;
	}
	private Env check_record(TypeDeclaratorsList x, Env cur_env) throws Exception
	{
		env = new Env(env);

		offsets.push(new Integer(offset));
		offset = 0;

		for(Type_declarators elem: x.type_declarators_list)
		{
			Type tp = check_typeSpecifier(elem.type, cur_env, 1);
			for(Declarator d: elem.declarators.declarator_list)
			{
				check_declarator(d, tp, false);
			}
		}
		
		Env res = env;
		env = env.prev;

		if(offset%4!=0) offset = (offset/4+1)*4;
		return res;
	}
	private Type check_typeName(Type_name x) throws Exception
	{
		Type cur = check_typeSpecifier(x.type, env, 0);
		for(int i=1;i<=x.star.total;++i)
		{
			cur = new Pointer(cur);
		}
		return cur;
	}
	private void recheck_record(TypeDeclaratorsList x, String name, Env e, Env cur_env) throws Exception
	{
		Env tmp = env;
		env = e;

		Type target = cur_env.get_all(name.intern()).type;
		for(Type_declarators elem: x.type_declarators_list)
		{
			Type tp = check_typeSpecifier(elem.type, cur_env, 0);
			if(tp==target)
			{
				for(Declarator d: elem.declarators.declarator_list)
				{
					VarEntry varEntry = (VarEntry) e.get(d.plaindecl.name.intern());
					int original_offset = varEntry.offset;
					e.table.remove(d.plaindecl.name.intern());
					check_declarator(d, tp, original_offset);
				}
			}
		}

		env = tmp;
	}
}
