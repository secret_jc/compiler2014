package appetizer.semantics;

import java.util.Hashtable;

public class Env
{
	public Env prev;
	public Hashtable<String, Entry> table;

	public Env(Env p)
	{
		this();
		prev = p;
	}
	public Env()
	{
		table = new Hashtable<String, Entry>();
	}
	public void add(String id, Entry e) throws Exception
	{
		Semantics.total_symbol++;  //only for debugging

		String u = id.intern();
		Entry res = table.get(u);
		if(res != null) throw new Exception(id+" has already been declared.");
		else table.put(id.intern(),e);
	}
	public boolean contains(String id)
	{
		return table.contains(id.intern());
	}
	public Entry get(String id)
	{
		return table.get(id.intern());
	}
	public Entry get_all(String id)
	{
		Env tmp=this; Entry ty=this.get(id.intern());
		if(ty!=null) return ty;
		while(tmp.prev!=tmp)
		{
			tmp=tmp.prev;
			ty=tmp.get(id.intern());
			if(ty!=null) return ty;
		}
		return null;
	}
}
