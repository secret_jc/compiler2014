package appetizer.semantics;

public final class Int extends Type
{
	private static Int res;

	public static Int getInstance()
	{
		if(res == null) 
		{
			res = new Int();
			return res;
		}
		else return res;
	}
}
