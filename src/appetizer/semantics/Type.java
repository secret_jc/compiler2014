package appetizer.semantics;
import appetizer.ast.*;

public abstract class Type
{
	//whether or not exactly the same
	public boolean equals(Type ob)
	{
		if(this == ob) return true;
		else if((this instanceof Int)&&(ob instanceof Char)) return true;
		else if((this instanceof Char)&&(ob instanceof Int)) return true;
		else if((this instanceof Name)&&(ob instanceof Name)) 
		{
			Name l = (Name) this, r = (Name) ob;
			return (l.name.intern()==r.name.intern())&&(l.env==r.env);
		}
		else if((this instanceof Pointer)&&(ob instanceof Pointer))
		{
			return ((Pointer) this).elementType.equals(((Pointer) ob).elementType);
		}
		else if((this instanceof Array)&&(ob instanceof Array))
		{
			Array l = (Array) this, r = (Array) ob;
			return l.elementType.equals(r.elementType);
		}
		else if((this instanceof Struct)&&(ob instanceof Struct))
		{
			Struct l = (Struct) this, r = (Struct) ob;
			return (l.name.intern()==r.name.intern())&&(l.env==r.env);
		}
		else if((this instanceof Union)&&(ob instanceof Union))
		{
			Union l = (Union) this, r = (Union) ob;
			return (l.name.intern()==r.name.intern())&&(l.env==r.env);
		}
		else return false;
	}

	//whether or not can operate binary
	public Type can_binary_operate(Double_expression.Op op, Type ob)
	{
		if((this instanceof Int)||(this instanceof Char))
		{
			if((ob instanceof Int)||(ob instanceof Char)) return Int.getInstance();
			else return null;
		}
		else if(this instanceof Pointer)
		{
			switch(op)
			{
				case EQ:
				case LE:
				case GE:
				case LT:
				case GT:
				case NE:
					if((ob instanceof Pointer)||(ob instanceof Array)||ob.equals(Int.getInstance())) return Int.getInstance();
					else return null;
				case MINUS:
					if(ob.equals(Int.getInstance())) {
						return this;
					}
					else if((ob instanceof Pointer)&&this.equals(ob)) {
						return Int.getInstance();
					}
					else return null;
				case PLUS:
					if(ob.equals(Int.getInstance())) return this;
					else return null;
				default:
					return null;
			}
		}
		else if(this instanceof Array)
		{
			switch(op)
			{
				case EQ:
				case LE:
				case GE:
				case LT:
				case GT:
				case NE:
					if((ob instanceof Pointer)||(ob instanceof Array)||ob.equals(Int.getInstance())) return Int.getInstance();
					else return null;
				case MINUS:
					if(ob.equals(Int.getInstance())) return this;
					else if((ob instanceof Array)&&this.equals(ob)) return Int.getInstance();
					else return null;
				case PLUS:
					if(ob.equals(Int.getInstance())) return this;
					else return null;
				default:
					return null;
			}

		}
		else return null;
	}

	//whether or not can do the assignment
	public Type can_assign_operate(Type obj)
	{
		if(this instanceof Array) 
		{
			if(obj instanceof Array_Init) return this;
			else return null;
		}
		else if(this.equals(obj)) return this;
		else if(this instanceof Pointer)
		{
			if(obj instanceof Pointer) return this;
			else if(obj instanceof Array) return this;
			else if(obj.equals(Int.getInstance())) return this;
			else return null;
		}
		else if(this.equals(Int.getInstance())&&((obj instanceof Pointer)||(obj instanceof Array))) return Int.getInstance();
		else if(this.equals(Void.getInstance())) return null;
		else return null;
	}
}
