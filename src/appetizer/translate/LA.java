package appetizer.translate;

import appetizer.intermediate.Label;
import appetizer.intermediate.Temp;

public class LA implements MIPS {
	
	Temp reg;
	Label label;

	public LA(Temp t, Label l) {
		reg = t;
		label = l;
	}
	
	public String toString() {
		return "\tla " + reg + ", " + label.Mips();
	}

}
