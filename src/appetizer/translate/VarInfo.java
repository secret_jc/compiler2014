package appetizer.translate;

/**
 * In a real project, you may need to add typing information here.
 */
public class VarInfo {

	public Temp2 loc;

	public VarInfo(int offset) {
		loc = new Temp2(offset);
	}
}
