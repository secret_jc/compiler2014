package appetizer.translate;

import appetizer.intermediate.Temp;

public class LI implements MIPS {

	Temp reg;
	int number;
	
	public LI(Temp t, int n) {
		reg = t;
		number = n;
	}
	
	public String toString() {
		return "\tli " + reg + ", " + number;
	}

}
