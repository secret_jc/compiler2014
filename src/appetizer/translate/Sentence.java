package appetizer.translate;

public class Sentence implements MIPS{
	
	String str;
	
	public Sentence(String s) {
		str = s;
	}

	public String toString() {
		return str;
	}
}
