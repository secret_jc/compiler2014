package appetizer.translate;

import appetizer.intermediate.Label;
import appetizer.intermediate.Temp;

public class Mips_beqz implements MIPS{
	
	Temp reg;
	Label label;

	public Mips_beqz(Temp t, Label l) {
		reg = t;
		label = l;
	}
	
	public String toString() {
		return "\tbeqz " + reg + ", " + label;
	}

}
