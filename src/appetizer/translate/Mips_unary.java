package appetizer.translate;

import appetizer.intermediate.Temp;

public class Mips_unary implements MIPS {

	Temp des, reg;
	String oper;
	
	public Mips_unary(String o, Temp t1, Temp t2) {
		oper = o;
		des = t1;
		reg = t2;
	}
	
	public String toString() {
		return "\t" + oper + " " + des + ", " + reg;
	}

}
