package appetizer.translate;

import appetizer.intermediate.Temp;

public class SW implements MIPS{
	public Temp t1, t2;
	public int base;
	public SW(Temp a1, Temp a2, int b) {
		t1 = a1;
		t2 = a2;
		base = b;
	//	print();
	}
	
	public void print() {
		if (t2.number<=0) {
			System.out.println("\tmove " + t2 + ", " + t1);
		}
		else {
			System.out.println("\tsw " + t1 + ", " + t2.Mips(base));
		}
	}
	
	public String toString() {
		if (t2.number<=0) {
			return "\tmove " + t2 + ", " + t1;
		}
		else {
			return "\tsw " + t1 + ", " + t2.Mips(base);
		}
		
	}
}
