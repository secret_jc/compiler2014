package appetizer.translate;

import appetizer.intermediate.Temp;

public class Mips_move implements MIPS {
	
	Temp des, reg;

	public Mips_move(Temp t1, Temp t2) {
		des = t1;
		reg = t2;
	}
	
	public String toString() {
		if (des.number != reg.number)
			return "\tmove " + des + ", " + reg;
		else 
			return "#move self";
	}

}
