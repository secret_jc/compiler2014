package appetizer.translate;

import appetizer.intermediate.Temp;

public class Mips_comp implements MIPS {
	
	Temp des, reg1, reg2;
	String oper;
	

	public Mips_comp(String o, Temp t0, Temp mark1, Temp mark2) {
		oper = o;
		des = t0;
		reg1 = mark1;
		reg2 = mark2;
	}

	public String toString() {
		return "\t" + oper + " " + des + ", " + reg1 + ", " + reg2;
	}

}
