package appetizer.translate;

import appetizer.intermediate.Label;
import appetizer.intermediate.Temp;

public class Mips_bnez implements MIPS{
	
	Temp reg;
	Label label;

	public Mips_bnez(Temp t, Label l) {
		reg = t;
		label = l;
	}
	
	public String toString() {
		return "\tbnez " + reg + ", "+label;
	}

}
