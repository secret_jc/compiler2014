package appetizer.translate;

import appetizer.intermediate.Temp;

public class Mips_binary implements MIPS {
	
	Temp des, reg1, reg2;
	int right;
	String oper;
	

	public Mips_binary(String o, Temp t0, Temp mark1, Temp mark2) {
		oper = o;
		des = t0;
		reg1 = mark1;
		reg2 = mark2;
	}
	
	public Mips_binary(String o, Temp t0, Temp mark1, int r) {
		oper = o;
		des = t0;
		reg1 = mark1;
		reg2 = null;
		right = r;
	}

	public String toString() {
		if (reg2 == null) {
			return "\t" + oper + " " + des + ", " + reg1 + ", " + right;
		}
		else
			return "\t" + oper + " " + des + ", " + reg1 + ", " + reg2;
	}

}
