package appetizer.main;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Hashtable;

import appetizer.optimize.*;
import appetizer.ast.*;
import appetizer.intermediate.*;
import appetizer.semantics.*;
import appetizer.syntactic.parser;

public class Main {

	public static String pathOf(String filename) {
		return Main.class.getResource(filename).getPath();
	}
	
	private static void compile(String filename) throws Exception
	{
		InputStream inp = new FileInputStream(filename);
		parser parser = new parser(inp);
		java_cup.runtime.Symbol root = null;
	
		try {
			root = parser.parse();
			Semantics res = new Semantics(root.value);
			res.check();
			Interpret ic = new Interpret(root.value, res.Flag);
			Intermediate code = ic.Interpret();
			
			Optimize_inter opt = new Optimize_inter(code);
		//	code.print();
			code = opt.Optimize();
		//	code.print();
		//	System.out.println("---------------------");
			Translate2 mips = new Translate2(code);
		//	Translate mips = new Translate(code);
			mips.translate();
		} catch (Exception e) {
			System.err.println(e.toString());
			System.exit(1);
		} 
		System.exit(0);
	}
	
	public static void main(String argv[]) throws Exception {
		compile(argv[0]);
		
	}
}
