package appetizer.ast;

public class Assignment_expression extends Expr
{
	public static enum Op
	{
		ASSIGN, MUL_ASSIGN, DIV_ASSIGN, MOD_ASSIGN, ADD_ASSIGN, SUB_ASSIGN, SHL_ASSIGN, SHR_ASSIGN, AND_ASSIGN, XOR_ASSIGN, OR_ASSIGN,
	}	
	
	public Op op;
	public Expr left, right;
	
	public Assignment_expression(Op o, Expr l, Expr r)
	{
		op = o;
		left = l;
		right = r;
	}
	public String toString()
	{
		return left.toString()+" "+getOp(op)+" "+right.toString();
	}

	public static String getOp(Op x)
	{
		switch(x)
		{
			case ASSIGN:
				return "=";
			case MUL_ASSIGN:
				return "*=";
			case DIV_ASSIGN:
				return "/=";
			case MOD_ASSIGN:
				return "%=";
			case ADD_ASSIGN:
				return "+=";
			case SUB_ASSIGN:
				return "-=";
			case SHL_ASSIGN:
				return "<<=";
			case SHR_ASSIGN:
				return ">>=";
			case AND_ASSIGN:
				return "&=";
			case XOR_ASSIGN:
				return "^=";
			case OR_ASSIGN:
				return "|=";
			default:
				return "";
		}
	}
	public static String getOpMid(Op x)
	{
		switch(x)
		{
			case MUL_ASSIGN:
				return "*";
			case DIV_ASSIGN:
				return "/";
			case MOD_ASSIGN:
				return "%";
			case ADD_ASSIGN:
				return "+";
			case SUB_ASSIGN:
				return "-";
			case SHL_ASSIGN:
				return "<<";
			case SHR_ASSIGN:
				return ">>";
			case AND_ASSIGN:
				return "&";
			case XOR_ASSIGN:
				return "^";
			case OR_ASSIGN:
				return "|";
			default:
				return "";
		}
	}
	public static Double_expression.Op Get_BinaryOp(Op x) {
		Double_expression.Op op;
		switch(x) {
			case MUL_ASSIGN:
				op = Double_expression.Op.TIMES;
				break;
			case DIV_ASSIGN:
				op = Double_expression.Op.DIVIDE;
				break;
			case MOD_ASSIGN:
				op = Double_expression.Op.MODULE;
				break;
			case ADD_ASSIGN:
				op = Double_expression.Op.PLUS;
				break;
			case SUB_ASSIGN:
				op = Double_expression.Op.MINUS;
				break;
			case SHL_ASSIGN:
				op = Double_expression.Op.SHL;
				break;
			case SHR_ASSIGN:
				op = Double_expression.Op.SHR;
				break;
			case AND_ASSIGN:
				op = Double_expression.Op.BIT_AND;
				break;
			case XOR_ASSIGN:
				op = Double_expression.Op.BIT_XOR;
				break;
			default: //OR_ASSIGN
				op = Double_expression.Op.BIT_OR;
				break;
		}
		return op;
	}
}
