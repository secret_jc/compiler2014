package appetizer.ast;

public class Double_expression extends Expr
{
	public static enum Op
	{
		OR, AND, BIT_OR, BIT_XOR, BIT_AND, EQ, NE, LT, GT, LE, GE, SHL, SHR, PLUS, MINUS, TIMES, DIVIDE, MODULE
	}	
	
	public Op op;
	public Expr left, right;
	
	public Double_expression(Op o, Expr l, Expr r)
	{
		op = o;
		left = l;
		right = r;
	}

	public String toString()
	{
		return left.toString()+" "+getOp(op)+" "+right.toString();
	}
	public int getNum()
	{
		int l,r;
		l = left.result;
		r = right.result;
		switch(op)
		{
			case OR:
				if((l!=0)||(r!=0)) return 1;
				else return 0;
			case AND:
				if((l!=0)&&(r!=0)) return 1;
				else return 0;
			case BIT_OR:
				return l|r;
			case BIT_AND:
				return l&r;
			case BIT_XOR:
				return l^r;
			case EQ:
				if(l==r) return 1;
				else return 0;
			case NE:
				if(l!=r) return 1;
				else return 0;
			case LT:
				if(l<r) return 1;
				else return 0;
			case GT:
				if(l>r) return 1;
				else return 0;
			case LE:
				if(l<=r) return 1;
				else return 0;
			case GE:
				if(l>=r) return 1;
				else return 0;
			case SHL:
				return l<<r;
			case SHR:
				return l>>r;
			case PLUS:
				return l+r;
			case MINUS:
				return l-r;
			case TIMES:
				return l*r;
			case DIVIDE:
				return l/r;
			default: //%
				return l%r;
		}	
	}
	public static String getOp(Op x)
	{
		switch(x)
		{
			case OR:
				return "||";
			case AND:
				return "&&";
			case BIT_OR:
				return "|";
			case BIT_AND:
				return "&";
			case BIT_XOR:
				return "^";
			case EQ:
				return "==";
			case NE:
				return "!=";
			case LT:
				return "<";
			case GT:
				return ">";
			case LE:
				return "<=";
			case GE:
				return ">=";
			case SHL:
				return "<<";
			case SHR:
				return ">>";
			case PLUS:
				return "+";
			case MINUS:
				return "-";
			case TIMES:
				return "*";
			case DIVIDE:
				return "/";
			default: //%
				return "%";
		}
	}
}
