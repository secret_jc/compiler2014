package appetizer.ast;

import appetizer.semantics.*;

public abstract class Expr
{
	public Entry entry;	//annotated AST, containing isLvalue and type
	public boolean isConst = false; //constant or not, constant should be calculated during compiler time
	public int result;

	public String toString()
	{
		return "EXPR";
	}
}
