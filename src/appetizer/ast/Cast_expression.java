package appetizer.ast;

public class Cast_expression extends Expr {
	public Expr expr;
	public Type_name type_name;

	public Cast_expression(Type_name t, Expr e) 	{
		type_name = t;
		expr = e;
	}
	public String toString() {
		return "(Cast) "+expr.toString();
	}
}
