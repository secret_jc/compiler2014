package appetizer.ast;

public class Declaration extends DeclOrDef {
	public Type_specifier type;
	public Init_declarators init_declarators;

	public Declaration(Type_specifier t, Init_declarators d) {
		type = t;
		init_declarators = d;
	}
	
	public Declaration(Type_specifier t) {
		type = t;
	}
}
