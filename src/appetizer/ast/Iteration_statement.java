package appetizer.ast;

public class Iteration_statement extends Statement {
	public IterationType type;
	public Expr expr1, expr2, expr3;
	public Statement stmt;

	public static enum IterationType
	{
		While, For
	}

	public Iteration_statement(Expr a, Expr b, Expr c, Statement s)
	{
		type = IterationType.For;
		expr1 = a;
		expr2 = b;
		expr3 = c;
		stmt = s;
	}
	public Iteration_statement(Expr a, Statement s)
	{
		type = IterationType.While;
		expr1 = a;
		stmt = s;
	}
}

