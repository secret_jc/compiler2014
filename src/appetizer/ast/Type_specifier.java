package appetizer.ast;

public class Type_specifier {
	public static enum Type
	{
		VOID, CHAR, INT, STRUCT, UNION
	}

	public Type type;
	public String id;
	public TypeDeclaratorsList type_declarators_list;
	
	public Type_specifier(Type t)
	{
		type = t;
	}
	public Type_specifier(Type t, String i, TypeDeclaratorsList x)
	{
		type = t;
		id = i;
		type_declarators_list = x;
	}
}
