package appetizer.ast;

public class Initializer {
	public Expr expr;
	public InitializerList initializer_list;

	public Initializer(Expr a)
	{
		expr = a;
	}
	public Initializer(InitializerList i)
	{
		initializer_list = i;
	}
}
