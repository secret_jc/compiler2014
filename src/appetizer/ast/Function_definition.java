package appetizer.ast;

import appetizer.semantics.*;

public class Function_definition extends DeclOrDef {
	public Type_specifier type;
	public Plain_declarator plaindecl;
	public Parameters para;
	public Compound_statement compstmt;

	public FuncEntry entry;

	public Function_definition(Type_specifier t, Plain_declarator p, Parameters pa, Compound_statement cs)
	{
		type = t;
		plaindecl = p;
		para = pa;
		compstmt = cs;
	}
	public Function_definition(Type_specifier t, Plain_declarator p, Compound_statement cs)
	{
		type = t;
		plaindecl = p;
		compstmt = cs;
		para = new Parameters();
	}
}
