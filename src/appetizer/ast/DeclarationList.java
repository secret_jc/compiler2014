package appetizer.ast;

import java.util.LinkedList;

public class DeclarationList {
	public LinkedList<Declaration> declarations;

	public DeclarationList() {
		declarations = new LinkedList<Declaration>();
	}
}
