package appetizer.ast;

import java.util.LinkedList;

public class Expression extends Expr {
	public static enum Op {
		COMMA
	}

	public final Op op = Op.COMMA;
	public Expr left, right;

	public Expression(Expr l, Expr r) {
		left = l;
		right = r;
	}
	public String toString() {
		return left.toString()+" ,"+right.toString();
	}
}
