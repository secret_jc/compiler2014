package appetizer.ast;

public class Plain_declaration {
	public Type_specifier type;
	public Declarator declarator;

	public Plain_declaration(Type_specifier t, Declarator d) {
		type = t;
		declarator = d;
	}
}
