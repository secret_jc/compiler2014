package appetizer.ast;

import java.util.LinkedList;

public class Arguments
{
	public LinkedList<Expr> exprs;

	public Arguments(Expr a)
	{
		exprs = new LinkedList<Expr>();
		exprs.add(a);
	}
	public Arguments()
	{
		exprs = new LinkedList<Expr>();
	}
}
