package appetizer.ast;

import java.util.LinkedList;
import appetizer.semantics.*;

public class Declarator {
	public Plain_declarator plaindecl;
	public Parameters para;
	public LinkedList<Expr> exprs;
	public int flag;

	public VarEntry entry;

	public Declarator(Plain_declarator p, Parameters pa) {
		flag = 1;
		plaindecl = p;
		para = pa;
	}
	public Declarator(Plain_declarator p) {
		flag = 1;
		plaindecl = p;
	}
	
	public Declarator(Plain_declarator p, int nonsense)	{
		flag = 2;
		plaindecl = p;
		exprs = new LinkedList<Expr>();
	}
}
