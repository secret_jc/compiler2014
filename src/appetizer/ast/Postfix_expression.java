package appetizer.ast;

public class Postfix_expression extends Expr
{
	public static enum Op
	{
		MPAREN, PAREN, DOT, PTR, INC, DEC
	}

	public Expr expr;
	public Op op;
	public Object obj;

	public Postfix_expression(Op o, Object t, Expr e)
	{
		expr = e;
		obj = t;
		op = o;
	}
	public String toString()
	{
		switch(op)
		{
			case MPAREN:
				return expr.toString()+"["+((Expr) obj).toString()+"]";
			case PAREN:
				return expr.toString()+"(Func Call)";
			case DOT:
				return expr.toString()+"."+(String)obj;
			case PTR:
				return expr.toString()+"->"+(String)obj;
			case INC:
				return expr.toString()+"++";
			case DEC:
				return expr.toString()+"--";
			default:
				return "";
		}
	}
}
