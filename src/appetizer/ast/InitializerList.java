package appetizer.ast;

import java.util.LinkedList;

public class InitializerList {
	public LinkedList<Initializer> initializer_list;

	public InitializerList(Initializer i) {
		initializer_list = new LinkedList<Initializer>();
		initializer_list.add(i);
	}
}
