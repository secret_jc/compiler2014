package appetizer.ast;

public class Init_declarator {
	public Declarator declarator;
	public Initializer initializer;

	public Init_declarator(Declarator d, Initializer i)
	{
		declarator = d;
		initializer = i;
	}
	public Init_declarator(Declarator d)
	{
		declarator = d;
	}
}
