package appetizer.ast;

public class Type_declarators
{
	public Type_specifier type;	
	public Declarators declarators;

	public Type_declarators(Type_specifier t, Declarators d)
	{
		type = t;
		declarators = d;
	}
}
