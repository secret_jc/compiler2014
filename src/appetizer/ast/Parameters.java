package appetizer.ast;

import java.util.LinkedList;

public class Parameters {
	public LinkedList<Plain_declaration> plain_declaration_list;
	
	public Parameters(Plain_declaration d) 	{
		plain_declaration_list = new LinkedList<Plain_declaration>();
		plain_declaration_list.add(d);
	}
	
	public Parameters() {
		plain_declaration_list = new LinkedList<Plain_declaration>();
	}
}
