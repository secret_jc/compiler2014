package appetizer.ast;

public class Jump_statement extends Statement 
{
	public JumpType type;
	public Expr expr;

	public static enum JumpType
	{
		Continue, Break, Return;
	}

	public Jump_statement(JumpType t)
	{
		type = t;
	}
	public Jump_statement(Expr e)
	{
		expr = e;
		type = JumpType.Return;
	}
}
