package appetizer.ast;

public class Unary_expression extends Expr {
	public Expr expr;
	public Op op;
	public Type_name typename;

	public static enum Op
	{
		INC, DEC, SIZEOF,
		BIT_AND, STAR, POSITIVE, NEGATIVE, BIT_NOT, NOT
	}

	public Unary_expression(Type_name t)
	{
		op = Op.SIZEOF;
		typename = t;
	}

	public Unary_expression(Op o, Expr e)
	{
		op = o;
		expr = e;
	}
	public String toString()
	{
		switch(op)
		{
			case INC:
				return "++"+expr.toString();
			case DEC:
				return "--"+expr.toString();
			case SIZEOF:
				return "SIZEOF";
			default:
				return getOp(op)+expr.toString();
		}
	}
	public static String getOp(Op x)
	{
		switch(x)
		{
			case BIT_NOT:
				return "~";
			case BIT_AND:
				return "&";
			case STAR:
				return "*";
			case POSITIVE:
				return "+";
			case NEGATIVE:
				return "-";
			case NOT:
				return "!";
			default:
				return "";
		}
	}
}
