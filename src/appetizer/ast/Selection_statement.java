package appetizer.ast;

public class Selection_statement extends Statement
{
	public Expr expr;
	public Statement thenstmt;
	public Statement elsestmt;

	public Selection_statement(Expr e, Statement s1, Statement s2)
	{
		expr = e;
		thenstmt = s1;
		elsestmt = s2;
	}
	public Selection_statement(Expr e, Statement s1)
	{
		expr = e;
		thenstmt = s1;
	}
}
