package appetizer.ast;

public class Primary_expression extends Expr
{
	public static enum Op
	{
		ID, STRING, INT, CHAR
	}

	public Op op;
	public Object obj;

	public Primary_expression(Op o, Object ob)
	{
		op = o;
		obj = ob;
	}
	public int getNum()
	{
		if(op == Op.INT)
		{
			Integer tmp = (Integer) obj;
			return tmp.intValue();
		}
		else //op = CHAR
		{
			Character tmp = (Character) obj;
			return (int)(tmp.charValue());
		}
	}
	public String toString()
	{
		switch(op)
		{
			case ID:
				return (String)obj;
			case STRING:
				return "\""+(String)obj+"\"";
			case INT:
				return ""+(Integer)obj;
			case CHAR:
				return "\'"+(Character)obj+"\'";
			default:
				return "";
		}
	}
}
