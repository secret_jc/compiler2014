package appetizer.ast;

public class Compound_statement extends Statement {
	public DeclarationList declaration_list;
	public StmtList stmt_list;

	public Compound_statement(DeclarationList d, StmtList s) {
		declaration_list = d;
		stmt_list = s;
	}
}
