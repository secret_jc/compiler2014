package appetizer.ast;

import java.util.LinkedList;

public class Init_declarators {
	public LinkedList<Init_declarator> init_declarator_list;

	public Init_declarators(Init_declarator i) {
		init_declarator_list = new LinkedList<Init_declarator>();
		init_declarator_list.add(i);
	}
}
