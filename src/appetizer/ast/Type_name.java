package appetizer.ast;

public class Type_name {
	public Star star;
	public Type_specifier type;

	public Type_name(Type_specifier t, Star s) {
		type = t;
		star = s;
	}
}
