package appetizer.functions;

import appetizer.semantics.*;
import java.util.*;

public class Sizeof
{
	public static int sizeof(Type x)
	{
		if(x instanceof Int) return 4;
		else if(x instanceof Char) return 1;
		else if(x instanceof Pointer) return 4;
		else if(x instanceof Array)
		{
			Array y = (Array) x;
			return y.capacity*Sizeof.sizeof(y.elementType);
		}
		else if(x instanceof Record) return ((Record) x).size;
		else return 0;
	}
	public static int init_func()
	{
		return 8;
	}
}

