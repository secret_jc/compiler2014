package appetizer.functions;

public class Printf {
	//public String str = "\nFunction_printf:\nli $t0, \'%\'\nlw $t1, 0($sp)\nmove $t5, $0\nLabel1:\nlb $t2, 0($t1)\nbeqz $t2, Label2\nbne $t2, $t0, other\nlb $t3, 1($t1)\nli $t4, \'d\'\nbeq $t3, $t4, integer\nli $t4, \'s\'\nbeq $t3, $t4, string\naddi $t5, $t5, 4\nadd $t6, $t5, $sp\nlb $a0, 0($t6)\nli $v0, 11\nsyscall\naddi $t1, $t1, 2\nj Label1\nstring:\naddi $t5, $t5, 4\nadd $t6, $t5, $sp\nlw $t7, 0($t6)\nLabel7:\nlb $t8, 0($t7)\nbeqz $t8, Label8\nmove $a0, $t8\nli $v0, 11\nsyscall\naddi $t7, $t7, 1\nj Label7\nLabel8:\naddi $t1, $t1, 2\nj Label1\ninteger:\naddi $t5, $t5, 4\nadd $t6, $t5, $sp\nlw $a0, 0($t6)\nli $v0, 1\nsyscall\naddi $t1, $t1, 2\nj Label1\nother:\nli $v0, 11\nmove $a0, $t2\nsyscall\naddi $t1, $t1, 1\nj Label1\nLabel2:\njr $ra\n";
	public String str = "\nFunction_printf:\n\tli $t0, '%'\n\tlw $t1, 0($sp)\n\tmove $t5, $0\nLL1:\n\tlb $t2, 0($t1)\n\tbeqz $t2, LL2\n\tbne $t2, $t0, LL3\n\tlb $t3, 1($t1)\n\tli $t4, 'd'\n\tbeq $t3, $t4, LL5\n\tli $t4, 's'\n\tbeq $t3, $t4, LL6\n\tli $t4, '0'\n\tbeq $t3, $t4, LL9\n#%c\n\taddi $t5, $t5, 4\n\tadd $t6, $t5, $sp\n\tlb $a0, 0($t6)\n\tli $v0, 11\n\tsyscall\n\taddi $t1, $t1, 2\n\tj LL1\nLL6:\n\t#%s\n\taddi $t5, $t5, 4\n\tadd $t6, $t5, $sp\n\tlw $t7, 0($t6)\nLL7:\n\tlb $t8, 0($t7)\n\tbeqz $t8, LL8\n\tmove $a0, $t8\n\tli $v0, 11\n\tsyscall\n\taddi $t7, $t7, 1\n\tj LL7\nLL8:\n\taddi $t1, $t1, 2\n\tj LL1\nLL5:\n\t#%d\n\taddi $t5, $t5, 4\n\tadd $t6, $t5, $sp\n\tlw $a0, 0($t6)\n\tli $v0, 1\n\tsyscall\n\taddi $t1, $t1, 2\n\tj LL1\nLL9:\n\t#%04d\n\tli $t0, 2\n\tli $t6, 1\n\taddi $t1, $t1, 2\n\tlb $t2, 0($t1)\n\tsub $t2, $t2, 48\n\taddi $t5, $t5, 4\n\tadd $t4, $t5, $sp\n\tlw $t3, 0($t4)\nG1:\n\tsle $t4, $t0, $t2\n\tbeqz $t4, G2\n\tmul $t6, $t6, 10\n\tslt $t4, $t3, $t6\n\tbeqz $t4, G4\n\tmove $a0, $0\n\tli $v0, 1\n\tsyscall\nG4:\n\tadd $t0, $t0, 1\n\tj G1\nG2:\n\tmove $a0, $t3\n\tli $v0, 1\n\tsyscall\n\taddi $t1, $t1, 2\n\tj LL1\nLL3:\n\t#normal\n\tli $v0, 11\n\tmove $a0, $t2\n\tsyscall\n\taddi $t1, $t1, 1\n\tj LL1\nLL2:\n\t#end\n\tjr $ra";
	
	public String toString()
	{
		return str;
	}
	public String mips()
	{
		return str;
	}
}
