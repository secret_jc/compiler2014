printf:
	li $t0, '%'
	lw $t1, 0($sp)
	move $t5, $0
LL1:
	lb $t2, 0($t1)
	beqz $t2, LL2
	bne $t2, $t0, LL3
	lb $t3, 1($t1)
	li $t4, 'd'
	beq $t3, $t4, LL5
	li $t4, 's'
	beq $t3, $t4, LL6
	li $t4, '0'
	beq $t3, $t4, LL9
#%c
	addi $t5, $t5, 4
	add $t6, $t5, $sp
	lb $a0, 0($t6)
	li $v0, 11
	syscall
	addi $t1, $t1, 2
	j LL1

LL6:	#%s
	addi $t5, $t5, 4
	add $t6, $t5, $sp
	lw $t7, 0($t6)
LL7:
	lb $t8, 0($t7)
	beqz $t8, LL8
	move $a0, $t8
	li $v0, 11
	syscall
	addi $t7, $t7, 1
	j LL7

LL8:
	addi $t1, $t1, 2
	j LL1

LL5:	#%d
	addi $t5, $t5, 4
	add $t6, $t5, $sp
	lw $a0, 0($t6)
	li $v0, 1
	syscall
	addi $t1, $t1, 2
	j LL1
	
LL9:	#%04d
	li $t0, 2
	li $t6, 1
	addi $t1, $t1, 2
	lb $t2, 0($t1)
	sub $t2, $t2, 48 
	addi $t5, $t5, 4
	add $t4, $t5, $sp
	lw $t3, 0($t4)
G1:
	sle $t4, $t0, $t2
	beqz $t4, G2
	mul $t6, $t6, 10
	slt $t4, $t3, $t6
	beqz $t4, G4
	move $a0, $0
	li $v0, 1
	syscall
G4:
	add $t0, $t0, 1
	j G1
G2:
	move $a0, $t3
	li $v0, 1
	syscall
	addi $t1, $t1, 2
	j LL1

LL3:	#normal
	li $v0, 11
	move $a0, $t2
	syscall
	addi $t1, $t1, 1
	j LL1

LL2:	#end
	jr $ra

