package appetizer.optimize;

import java.util.HashSet;
import java.util.Iterator;

import appetizer.intermediate.*;
import appetizer.translate.LA;
import appetizer.translate.LB;
import appetizer.translate.LI;
import appetizer.translate.LW;
import appetizer.translate.Mips_beqz;
import appetizer.translate.Mips_binary;
import appetizer.translate.Mips_bnez;
import appetizer.translate.Mips_comp;
import appetizer.translate.Mips_move;
import appetizer.translate.Mips_unary;
import appetizer.translate.SB;
import appetizer.translate.SW;
import appetizer.translate.Sentence;

public class Optimize_inter {
	public Intermediate Inter, copy;
	Quad list[] = new Quad[2500];
	int Num_T = 0; //
	int total = 0; //
	int edge;
	int head[] = new int[2500];
	int point[] = new int[2500];
	int prev[] = new int[2500];
	int start[] = new int[2310];
	int end[] = new int[2310];
	Info sent[] = new Info[2500];
	HashSet<Integer> reg = new HashSet<Integer>();
	int tmp[] = new int[2310];
	
	public Optimize_inter(Intermediate inter) {
		Inter = inter;
		//System.out.println(Inter.malloc);
	}
	
	public Intermediate Optimize() { //do all the optimization
		if (Inter.count < 100) 
			reg_allocate();
		return Inter;
	}
	
	void initialize() {
		edge = 0;
		for (int i=1;i<=total;i++) head[i] = 0;
		
		reg.clear();
		reg.add(5);
		reg.add(6);
		reg.add(7);
		for (int i=15;i<=25;i++) 
			reg.add(i);
		
		for (int i=1;i<=Num_T;i++) tmp[i] = 0;
		total = 0;
	}
	
	void Get_block() {
	}
	
	int Get_label(Label l) {
		for (int i=1;i<=total;i++)
			if (list[i] instanceof Label_quad) {
				Label_quad t = (Label_quad) list[i];
				if (l.name.intern() == t.label.name.intern())
					return i;
			}
		return 0;
	}
	
	void insert(int a, int b) {
		edge = edge + 1;
		point[edge] = b;
		prev[edge] = head[a];
		head[a] = edge;
	}
	
	boolean Run_away(Quad x) {
		if (x instanceof Jump) return true;
		if (x instanceof Return) return true;
		return false;
	}
	
	void Get_graph() {
		Quad x;
		for (int i=1;i<=total;i++) {
			x = list[i];
			if (i<total && !(Run_away(list[i])) ) insert(i,i+1);
			if (x instanceof Beqz) {
				Beqz t = (Beqz) x;
				insert(i,Get_label(t.label));
			}
			else if (x instanceof Bnez) {
				Bnez t = (Bnez) x;
				insert(i,Get_label(t.label));
			}
			else if (x instanceof Jump) {
				Jump t = (Jump) x;
				insert(i,Get_label(t.label));
			}
		}
	}
	
	void update_def(int index, Temp t) {
		if (t == null) return;
		if (t.number>0) 
			sent[index].in.remove(t.number);
	}
	
	void update_use(int index, Temp t) {
		if (t == null) return;
		if (t.number>0) 
			sent[index].in.add(t.number);
	}
	
	void Get_live() {
		Quad x;
		for (int g=1;g<=300;g++) {               //
			for (int i=1;i<=total;i++) {
				sent[i].in.clear();         // in[i] <- out[i]
				for(Iterator it=sent[i].out.iterator();it.hasNext();) 
					sent[i].in.add((Integer) it.next());        //

				x = list[i];       // in[i] <-  -def[i] +use[i]
				if (x instanceof Beqz) {
					Beqz t = (Beqz) x;
					update_use(i, t.reg);
				}
				else if (x instanceof Binary) {
					Binary t = (Binary) x;
					update_def(i,t.res);
					update_use(i,t.left);
					update_use(i,t.right);
				}
				else if (x instanceof Binary_Imm) {
					Binary_Imm t = (Binary_Imm) x;
					update_def(i,t.res);
					update_use(i,t.left);
				}
				else if (x instanceof Bnez) {
					Bnez t = (Bnez) x;
					update_use(i,t.reg);
				}
				else if (x instanceof Comp) {
					Comp t = (Comp) x;
					update_def(i,t.res);
					update_use(i,t.reg1);
					update_use(i,t.reg2);
				}
				else if (x instanceof Comp_ch) {
					Comp_ch t = (Comp_ch) x;
					update_def(i,t.res);
					update_use(i,t.reg1);
					update_use(i,t.reg1);
				}
				else if (x instanceof Use_func) {
					Use_func t = (Use_func) x;
					update_def(i, t.res);
				}
				else if (x instanceof Malloc) {
					Malloc t = (Malloc) x;
					update_def(i,t.add);
					update_use(i,t.tmp);
				}
				else if (x instanceof Load_addr) {
					Load_addr t = (Load_addr) x;
					update_def(i,t.reg);
				}
				else if (x instanceof Load_word) {      
					Load_word t = (Load_word) x;
					update_def(i,t.res);
					update_use(i,t.add);
				}
				else if (x instanceof Load_byte) {       
					Load_byte t = (Load_byte) x;
					update_def(i,t.res);
					update_use(i,t.add);
				}
				else if (x instanceof Load_imm) {
					Load_imm t = (Load_imm) x;
					update_def(i,t.res);
				}
				else if (x instanceof LoadB_imm) {
					LoadB_imm t = (LoadB_imm) x;
					update_def(i,t.res);
				}
				else if (x instanceof Move) {
					Move t = (Move) x;
					update_def(i,t.res);
					update_use(i,t.ori);
				}
				else if (x instanceof Send_param) {
					Send_param t = (Send_param) x;
					update_use(i,t.reg);
				}
				else if (x instanceof Return) {
					Return t = (Return) x;
					if (t.reg!=null)
						update_use(i,t.reg);
				}
				else if (x instanceof Store_byte) {     
					Store_byte t = (Store_byte) x;
					update_use(i,t.add);
					update_use(i,t.reg);
				}
				else if (x instanceof Store_word) {
					Store_word t = (Store_word) x;
					update_use(i,t.add);
					update_use(i,t.reg);
				}
				else if (x instanceof Receive_param) {
					Receive_param t = (Receive_param) x;
					update_def(i,t.res);
				}
				else if (x instanceof Unary) {
					Unary t = (Unary) x;
					update_def(i,t.res);
					update_use(i,t.reg);
				}
				else if (x instanceof Address) {
					Address t = (Address) x;
					update_use(i,t.ori);
					update_def(i,t.res);
				}
				

				int now = head[i];          //  out[i] <- union of succ in[s]
				sent[i].out.clear();
				while (now>0) {
					int v = point[now];
					for(Iterator it=sent[v].in.iterator();it.hasNext();) 
						sent[i].out.add((Integer) it.next());        //
					now = prev[now];
				}
			}
		}
	}
	
	void update(int index, Temp t) {
		if (t == null) return;
		if (t.number>0) {
			sent[index].in.add(t.number);
			end[t.number] = index;
			if (start[t.number] == 0) start[t.number] = index;
		}
	}
	
	void Compute_length2() {
		Quad x;
		for (int i=1;i<=total;i++) {
			sent[i].in.clear();         
			x = list[i];      
			if (x instanceof Beqz) {
				Beqz t = (Beqz) x;
				update(i, t.reg);
			}
			else if (x instanceof Binary) {
				Binary t = (Binary) x;
				update(i,t.res);
				update(i,t.left);
				update(i,t.right);
			}
			else if (x instanceof Binary_Imm) {
				Binary_Imm t = (Binary_Imm) x;
				update(i,t.res);
				update(i,t.left);
			}
			else if (x instanceof Bnez) {
				Bnez t = (Bnez) x;
				update(i,t.reg);
			}
			else if (x instanceof Comp) {
				Comp t = (Comp) x;
				update(i,t.res);
				update(i,t.reg1);
				update(i,t.reg2);
			}
			else if (x instanceof Comp_ch) {
				Comp_ch t = (Comp_ch) x;
				update(i,t.res);
				update(i,t.reg1);
				update(i,t.reg1);
			}
			else if (x instanceof Use_func) {
				Use_func t = (Use_func) x;
				update(i, t.res);
			}
			else if (x instanceof Malloc) {
				Malloc t = (Malloc) x;
				update(i,t.add);
				update(i,t.tmp);
			}
			else if (x instanceof Load_addr) {
				Load_addr t = (Load_addr) x;
				update(i,t.reg);
			}
			else if (x instanceof Load_word) {      
				Load_word t = (Load_word) x;
				update(i,t.res);
				update(i,t.add);
			}
			else if (x instanceof Load_byte) {       
				Load_byte t = (Load_byte) x;
				update(i,t.res);
				update(i,t.add);
			}
			else if (x instanceof Load_imm) {
				Load_imm t = (Load_imm) x;
				update(i,t.res);
			}
			else if (x instanceof LoadB_imm) {
				LoadB_imm t = (LoadB_imm) x;
				update(i,t.res);
			}
			else if (x instanceof Move) {
				Move t = (Move) x;
				update(i,t.res);
				update(i,t.ori);
			}
			else if (x instanceof Send_param) {
				Send_param t = (Send_param) x;
				update(i,t.reg);
			}
			else if (x instanceof Return) {
				Return t = (Return) x;
				if (t.reg!=null)
					update(i,t.reg);
			}
			else if (x instanceof Store_byte) {     
				Store_byte t = (Store_byte) x;
				update(i,t.add);
				update(i,t.reg);
			}
			else if (x instanceof Store_word) {
				Store_word t = (Store_word) x;
				update(i,t.add);
				update(i,t.reg);
			}
			else if (x instanceof Receive_param) {
				Receive_param t = (Receive_param) x;
				update(i,t.res);
			}
			else if (x instanceof Unary) {
				Unary t = (Unary) x;
				update(i,t.res);
				update(i,t.reg);
			}
			else if (x instanceof Address) {
				Address t = (Address) x;
				update(i,t.ori);
				update(i,t.res);
			}
		}
	}
	
	void Show() {
		for (int i=1;i<=total;i++) {
			System.out.println(list[i]);
			for(Iterator it=sent[i].in.iterator();it.hasNext();) 
				System.out.print(it.next() + ", ");
			System.out.println("");
			for(Iterator it=sent[i].out.iterator();it.hasNext();) 
				System.out.print(it.next() + ", ");
			System.out.println("");
		}
	}
	
	void Compute_length() {
		int now;
		for (int i=1;i<=total;i++)
			for(Iterator it=sent[i].in.iterator();it.hasNext();) {
				now = (Integer) it.next();
				end[now] = i;
				if (now>Num_T) Num_T = now;
			}
		for (int i=total;i>=1;i--)
			for(Iterator it=sent[i].out.iterator();it.hasNext();) {
				now = (Integer) it.next();
				start[now] = i;
				if (now>Num_T) Num_T = now;
			}
	//	for (int i=1;i<=10;i++)     //show
		//	System.out.println(i+" "+start[i]+" "+end[i]);
	}
	
	public void Alloc(int now) {
		if (reg.isEmpty()) return;
		int res = reg.iterator().next();
		tmp[now] = res;
		reg.remove(res);
		//System.out.println(now + "  " + res);
	}

	private void Dealloc(int now) {
		reg.add(tmp[now]);
	}
	
	public void Allocate2() {
		for (int i=1;i<=total;i++) {
			for(Iterator it=sent[i].in.iterator();it.hasNext();)  {
				int num = (Integer) it.next();
				if (num>Num_T) Num_T = num;
				if (start[num] == i) Alloc(num);
				if (end[num] == i) Dealloc(num);
			}
		}
	}
	
	public void Allocate() {
		for (int i=1;i<=total;i++) {
			for(Iterator it=sent[i].out.iterator();it.hasNext();)  {
				int num = (Integer) it.next();
				if (start[num] == i) Alloc(num);
			}
			for(Iterator it=sent[i].in.iterator();it.hasNext();)  {
				int num = (Integer) it.next();
				if (end[num] == i) Dealloc(num);
			}
		}
	}
	
	Temp Get_reg(Temp t) {
		if (t == null) return null;
		if (t.number<=0) return t;
		if (tmp[t.number] == 0) return t;
		return (new Temp(-tmp[t.number]));
	}
	
	public void Rewrite() {
		Quad x;
		for (int i=1;i<=total;i++) {
			x = list[i];
			if (x instanceof Beqz) {
				Beqz t = (Beqz) x;
				t.reg = Get_reg(t.reg);
				copy.add(t);
			}
			else if (x instanceof Binary) {
				Binary t = (Binary) x;
				t.left = Get_reg(t.left);
				t.right = Get_reg(t.right);
				t.res = Get_reg(t.res);
				copy.add(t);
			}
			else if (x instanceof Binary_Imm) {
				Binary_Imm t = (Binary_Imm) x;
				t.left = Get_reg(t.left);
				t.res = Get_reg(t.res);
				copy.add(t);
			}
			else if (x instanceof Bnez) {
				Bnez t = (Bnez) x;
				t.reg = Get_reg(t.reg);
				copy.add(t);
			}
			else if (x instanceof Comp) {
				Comp t = (Comp) x;
				t.reg1 = Get_reg(t.reg1);
				t.reg2 = Get_reg(t.reg2);
				t.res = Get_reg(t.res);
				copy.add(t);
			}
			else if (x instanceof Comp_ch) {
				Comp_ch t = (Comp_ch) x;
				t.reg1 = Get_reg(t.reg1);
				t.reg2 = Get_reg(t.reg2);
				t.res = Get_reg(t.res);
				copy.add(t);
			}
			else if (x instanceof Use_func) {   //char
				Use_func t = (Use_func) x;

				for(Iterator it=sent[i].in.iterator();it.hasNext();)  {
					int p = (Integer) it.next();
					if (tmp[p]!=0) {
						Temp t0 = new Temp(-tmp[p]);
						Temp t1 = new Temp(p);
						copy.add(new Store_w(t0,t1));
					}
				}
				t.res = Get_reg(t.res);
				copy.add(t);
				
				for(Iterator it=sent[i].in.iterator();it.hasNext();)  {
					int p = (Integer) it.next();
					if (tmp[p]!=0) {
						Temp t0 = new Temp(-tmp[p]);
						Temp t1 = new Temp(p);
						copy.add(new Load_w(t0,t1));
					}
				}
			}
			else if (x instanceof Malloc) {
				Malloc t = (Malloc) x;
				t.tmp = Get_reg(t.tmp);
				t.add = Get_reg(t.add);
				copy.add(t);
			}
			else if (x instanceof Jump) {
				Jump t = (Jump) x;
				copy.add(t);
			}
			else if (x instanceof Label_quad) {
				Label_quad t = (Label_quad) x;
				copy.add(t);
			}
			else if (x instanceof Load_addr) {
				Load_addr t = (Load_addr) x;
				t.reg = Get_reg(t.reg);
				copy.add(t);
			}
			else if (x instanceof Load_word) {      //
				Load_word t = (Load_word) x;
				t.add = Get_reg(t.add);
				t.res = Get_reg(t.res);
				copy.add(t);
			}
			else if (x instanceof Load_byte) {       //
				Load_byte t = (Load_byte) x;
				t.add = Get_reg(t.add);
				t.res = Get_reg(t.res);
				copy.add(t);
			}
			else if (x instanceof Load_imm) {
				Load_imm t = (Load_imm) x;
				t.res = Get_reg(t.res);
				copy.add(t);
			}
			else if (x instanceof LoadB_imm) {
				LoadB_imm t = (LoadB_imm) x;
				t.res = Get_reg(t.res);
				copy.add(t);
			}
			else if (x instanceof Move) {
				Move t = (Move) x;
				t.ori = Get_reg(t.ori);
				t.res = Get_reg(t.res);
				copy.add(t);
			}
			else if (x instanceof Send_param) {
				Send_param t = (Send_param) x;
				t.reg = Get_reg(t.reg);
				copy.add(t);
			}
			else if (x instanceof Return) {
				Return t = (Return) x;
				if (t.reg!=null)
					t.reg = Get_reg(t.reg);
				copy.add(t);
			}
			else if (x instanceof Store_byte) {     
				Store_byte t = (Store_byte) x;
				t.add = Get_reg(t.add);
				t.reg = Get_reg(t.reg);
				copy.add(t);
			}
			else if (x instanceof Store_word) {
				Store_word t = (Store_word) x;
				t.add = Get_reg(t.add);
				t.reg = Get_reg(t.reg);
				copy.add(t);
			}
			else if (x instanceof End_func) {
				copy.add(x);
			}
			else if (x instanceof Receive_param) {
				Receive_param t = (Receive_param) x;
				if (sent[i].out.contains(t.res.number) || t.res.number<=0) {
					t.res = Get_reg(t.res);
					copy.add(t);
				}
			}
			else if (x instanceof Unary) {
				Unary t = (Unary) x;
				t.reg = Get_reg(t.reg);
				t.res = Get_reg(t.res);
				copy.add(t);
			}
			else if (x instanceof Address) {
				Address t = (Address) x;
				t.ori = Get_reg(t.ori);
				t.res = Get_reg(t.res);
				copy.add(t);
			}
			else if (x instanceof Gap) {
				
			}
			//System.out.println(x);
		}
	}

	public void reg_allocate() {
		copy = new Intermediate();
		initialize();
		for (Quad x:Inter.list) {
			if (!(x instanceof Gap)) {
				total = total + 1;
				list[total] = x;
				sent[total] = new Info();
			}
			if (x instanceof End_func) {
				if (!Inter.expr) {
					Get_block();
					Get_graph();
					Get_live();
					//	Show();
					Compute_length();
					Allocate();
				}
				else {
					Compute_length2();
					Allocate2();
				}
				Address_cancel();
				Rewrite();
				initialize();
			}
		}
		Inter.list = copy.list;
	}
	
	public void Address_cancel() {      //
		for (int i=1;i<=total;i++) {
			Quad x = list[i];
			if (x instanceof Address) {
				Address t = (Address) x;
				tmp[t.ori.number] = 0;
				tmp[t.res.number] = 0;
			}
		}
	}
	
}
