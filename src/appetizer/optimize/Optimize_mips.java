package appetizer.optimize;

import java.util.LinkedList;

import appetizer.intermediate.*;
import appetizer.translate.*; 

public class Optimize_mips {
	public LinkedList<MIPS> mips;
	
	public Optimize_mips(LinkedList<MIPS> m) {
		mips = m;
	}
	
	public LinkedList<MIPS> Optimize() { //do all the optimization
		Optimize_Load_Store();
		return mips;
	}
	
	public void Optimize_Load_Store() {
		boolean flag = false;
		Temp t1 = new Temp(-100), t2 = new Temp(-100);
		LinkedList<MIPS> fake = new LinkedList<MIPS>();
		fake.clear();
		for(MIPS x: mips) {
			if (x instanceof SW || x instanceof LW) {
				if (x instanceof SW) {
					SW t = (SW) x;
					if (flag && ((t.t1.number == t1.number && t.t2.number == t2.number) ||
						         (t.t1.number == t2.number && t.t2.number == t1.number)) ) {
						
					}
					else {
						t1 = t.t1;
						t2 = t.t2;
						fake.add(t);
					}
				}
				
				if (x instanceof LW) {
					LW t = (LW) x;
					if (flag && ((t.t1.number == t1.number && t.t2.number == t2.number) ||
						         (t.t1.number == t2.number && t.t2.number == t1.number)) ) {
						
					}
					else {
						t1 = t.t1;
						t2 = t.t2;
						fake.add(t);
					}
				}
				
				flag = true;
			}
			else {
				fake.add(x);
				flag = false;
			}
		}
		mips = fake;
	}
	
}
