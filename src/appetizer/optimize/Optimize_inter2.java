package appetizer.optimize;

import appetizer.intermediate.*;
import appetizer.translate.LA;
import appetizer.translate.LB;
import appetizer.translate.LI;
import appetizer.translate.LW;
import appetizer.translate.Mips_beqz;
import appetizer.translate.Mips_binary;
import appetizer.translate.Mips_bnez;
import appetizer.translate.Mips_comp;
import appetizer.translate.Mips_move;
import appetizer.translate.Mips_unary;
import appetizer.translate.SB;
import appetizer.translate.SW;
import appetizer.translate.Sentence;

public class Optimize_inter2 {
	public Intermediate Inter, copy;
	Temp tem[] = new Temp[8];
	int reg[] = new int[40];
	int loc[] = new int[5000];
	int start[] = new int[5000];
	int end[] = new int[5000];
	Quad list[] = new Quad[5000];
	int total = 0;
	
	public Optimize_inter2(Intermediate inter) {
		Inter = inter;
	}
	
	public Intermediate Optimize() { //do all the optimization
		//temp_var();
		reg_allocate();
		return Inter;
	}
	
	void initialize() {
		copy = new Intermediate();
		for (int i=1;i<=4000;i++) {
			start[i] = -1;
			end[i] = -1;
		}
	}
	
	
	public void reg_allocate() {
		initialize();
		boolean flag = false;
		total = 0;
		for (Quad x: Inter.list) {
			total = total + 1;
			list[total] = x;
		}
	}
	/*
	public int avail() {
		if (tem[5] == null) return 5;
		if (tem[6] == null) return 6;
		tem[5].reg = 1;
		return 5;
	}
	
	public void temp_var() {
		tem[5] = null;
		tem[6] = null;
		for (Quad x: Inter.list) {
			if (x instanceof Beqz) {
				Beqz t = (Beqz) x;
				tem[t.reg.reg] = null;
			}
			else if (x instanceof Binary) {
				Binary t = (Binary) x;
				tem[t.left.reg] = null;
				tem[t.right.reg] = null;
				if (t.res.temp) {
					t.res.reg = avail();
					tem[t.res.reg] = t.res;
				}
			}
			else if (x instanceof Binary_Imm) {
				Binary_Imm t = (Binary_Imm) x;
				tem[t.left.reg] = null;
				if (t.res.temp) {
					t.res.reg = avail();
					tem[t.res.reg] = t.res;
				}
			}
			else if (x instanceof Bnez) {
				Bnez t = (Bnez) x;
				tem[t.reg.reg] = null;
			}
			else if (x instanceof Comp) {
				Comp t = (Comp) x;
				tem[t.reg1.reg] = null;
				tem[t.reg2.reg] = null;
				if (t.res.temp) {
					t.res.reg = avail();
					tem[t.res.reg] = t.res;
				}
			}
			else if (x instanceof Comp_ch) {
				Comp_ch t = (Comp_ch) x;
				tem[t.reg1.reg] = null;
				tem[t.reg2.reg] = null;
				if (t.res.temp) {
					t.res.reg = avail();
					tem[t.res.reg] = t.res;
				}
			}
			else if (x instanceof Use_func) {
				Use_func t = (Use_func) x;
				if (tem[5] != null) tem[5].reg = 1;
				if (tem[6] != null)	tem[6].reg = 1;
			}
			else if (x instanceof Malloc) {
				Malloc t = (Malloc) x;
			}
			else if (x instanceof Jump) {
				Jump t = (Jump) x;
			}
			else if (x instanceof Label_quad) {
				Label_quad t = (Label_quad) x;
				if (t.label.func!=null) {
					tem[5] = null;
					tem[6] = null;
				}
			}
			else if (x instanceof Load_addr) {
				Load_addr t = (Load_addr) x;
				if (t.reg.temp) {
					t.reg.reg = avail();
					tem[t.reg.reg] = t.reg;
				}
			}
			else if (x instanceof Load_byte) {     
				Load_byte t = (Load_byte) x;
				tem[t.add.reg] = null;
				if (t.res.temp) {
					t.res.reg = avail();
					tem[t.res.reg] = t.res;
				}
			}
			else if (x instanceof Load_imm) {
				Load_imm t = (Load_imm) x;
				if (t.res.temp) {
					t.res.reg = avail();
					tem[t.res.reg] = t.res;
				}
			}
			else if (x instanceof LoadB_imm) {
				LoadB_imm t = (LoadB_imm) x;
				if (t.res.temp) {
					t.res.reg = avail();
					tem[t.res.reg] = t.res;
				}
			}
			else if (x instanceof Load_word) {
				Load_word t = (Load_word) x;
				tem[t.add.reg] = null;
				if (t.res.temp) {
					t.res.reg = avail();
					tem[t.res.reg] = t.res;
				}
			}
			else if (x instanceof Move) {
				Move t = (Move) x;
				tem[t.ori.reg] = null;
				if (t.res.temp) {
					t.res.reg = avail();
					tem[t.res.reg] = t.res;
				}
			}
			else if (x instanceof Send_param) {
				Send_param t = (Send_param) x;
				if (t.size == 1) {
					tem[t.reg.reg] = null;
				}
				else {
					tem[t.reg.reg] = null;
				}
			}
			else if (x instanceof Return) {
				Return t = (Return) x;
				if (t.reg !=null) {
					tem[t.reg.reg] = null;
				}
			}
			else if (x instanceof Store_byte) {     // ??
				Store_byte t = (Store_byte) x;
				tem[t.add.reg] = null;
				tem[t.reg.reg] = null;
			}
			else if (x instanceof Store_word) {
				Store_word t = (Store_word) x;
				tem[t.add.reg] = null;
				tem[t.reg.reg] = null;
			}
			else if (x instanceof End_func) {
			}
			else if (x instanceof Receive_param) {
				Receive_param t = (Receive_param) x;
				if (t.size == 1) {
					if (t.res.temp) {
						t.res.reg = avail();
						tem[t.res.reg] = t.res;
					}
				}
				else {
					if (t.res.temp) {
						t.res.reg = avail();
						tem[t.res.reg] = t.res;
					}
				}
			}
			else if (x instanceof Unary) {
				Unary t = (Unary) x;
				tem[t.reg.reg] = null;
				if (t.res.temp) {
					t.res.reg = avail();
					tem[t.res.reg] = t.res;
				}
			}
			else if (x instanceof Address) {
				Address t = (Address) x;
				tem[t.ori.reg] = null;
				if (t.res.temp) {
					t.res.reg = avail();
					tem[t.res.reg] = t.res;
				}
				
			}
			else if (x instanceof Gap) {
				
			}
			else {
			}
			//System.out.println(x);
		};
	}*/
}
