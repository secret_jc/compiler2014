package appetizer.optimize;

import java.util.HashSet;

public class Info {
	HashSet<Integer> in;
	HashSet<Integer> out;
	
	public Info() {
		in = new HashSet<Integer>();
		out = new HashSet<Integer>();
		in.clear();
		out.clear();
	}
	
}
