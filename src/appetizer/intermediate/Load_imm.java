package appetizer.intermediate;

public class Load_imm implements Quad
{
	public Temp res;
	public int number;

	public Load_imm(Temp r, int n)
	{
		number = n;
		res = r;
	}
	public String toString()
	{
		return res+" = "+number;
	}
}
