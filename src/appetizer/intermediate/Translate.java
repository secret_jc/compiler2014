package appetizer.intermediate;

import java.util.LinkedList;

import appetizer.ast.*;
import appetizer.functions.Printf;
import appetizer.optimize.Optimize_mips;
import appetizer.translate.*;

public class Translate {
	
	public Intermediate Inter;
	public int base, Parasize, Total_temp;
	public LinkedList<MIPS> mips = new LinkedList<MIPS>();
	public Optimize_mips opt;
	
	public Translate(Intermediate inter) {
		Inter = inter;
		base = 0;
		Parasize = Inter.MaxParaSize;            //
		mips.clear();
	}
	
	public void translate() {
		System.out.println("\t.data");
		for(Document x: Inter.documents) translate(x);
		System.out.println("\t.text");
		System.out.println("\t.align 2");
		System.out.println("\t.globl main");
		for(Quad x: Inter.list) translate(x);
		opt = new Optimize_mips(mips);
		mips = opt.Optimize();
		for(MIPS x: mips) System.out.println(x);
		Printf print = new Printf();
		System.out.println(print.mips());
	}

	private void translate(Quad x) {
		if (x instanceof Beqz) {
			Beqz t = (Beqz) x;
			mips.add(new LW(new Temp(-8), t.reg, base));
			mips.add(new Sentence(t.Mips(base)));
		}
		else if (x instanceof Binary) {
			Binary t = (Binary) x;
			mips.add(new LW(new Temp(-8), t.left, base));
			mips.add(new LW(new Temp(-9), t.right, base));
			mips.add(new Sentence("\t" + t.oper()+" $t0, $t0, $t1"));    
			mips.add(new SW(new Temp(-8), t.res, base));
		}
		else if (x instanceof Binary_Imm) {
			Binary_Imm t = (Binary_Imm) x;
			mips.add(new LW(new Temp(-8), t.left, base));
			mips.add(new Sentence("\t" + t.oper()+" $t0, $t0, "+t.right));
			mips.add(new SW(new Temp(-8), t.res, base));
		}
		else if (x instanceof Bnez) {
			Bnez t = (Bnez) x;
			mips.add(new LW(new Temp(-8), t.reg, base));
			mips.add(new Sentence("\tbnez $t0, "+t.label));
		}
		else if (x instanceof Comp) {
			Comp t = (Comp) x;
			mips.add(new LW(new Temp(-8), t.reg1, base));
			mips.add(new LW(new Temp(-9), t.reg2, base));
			mips.add(new Sentence("\t"+t.oper()+" $t0, $t0, $t1"));
			mips.add(new SW(new Temp(-8), t.res, base));
		}
		else if (x instanceof Comp_ch) {
			Comp_ch t = (Comp_ch) x;
			mips.add(new LB(new Temp(-8), t.reg1, base));
			mips.add(new LB(new Temp(-9), t.reg2, base));
			mips.add(new Sentence("\t"+t.oper()+" $t0, $t0, $t1"));
			mips.add(new SW(new Temp(-8), t.res, base));
		}
		else if (x instanceof Use_func) {
			Use_func t = (Use_func) x;
			mips.add(new Sentence("\tjal " + "Function_" + t.name));
			if (t.res != null) {
				mips.add(new SW(new Temp(-2), t.res, base));
			}
		}
		else if (x instanceof Malloc) {
			Malloc t = (Malloc) x;
			mips.add(new LW(new Temp(-4), t.tmp, base));
			mips.add(new Sentence("\tli, $v0 9"));
			mips.add(new Sentence("\tsyscall"));
			mips.add(new SW(new Temp(-2), t.add, base));
		}
		else if (x instanceof Jump) {
			Jump t = (Jump) x;
			mips.add(new Sentence(t.Mips()));
		}
		else if (x instanceof Label_quad) {
			Label_quad t = (Label_quad) x;
			if (t.label.func==null)
				mips.add(new Sentence(t.Mips()));
			else {
				base = 8 + t.label.func.varSize;
				Total_temp = t.label.func.tempSize;
				mips.add(new Sentence(t.Mips()));
				mips.add(new Sentence("\tsw $fp, -4($sp)"));
				mips.add(new Sentence("\tsw $ra, -8($sp)"));
				mips.add(new Sentence("\tmove $fp, $sp"));
				mips.add(new Sentence("\tsub $sp, $sp, " + (base + t.label.func.paraSize + Total_temp + Parasize + 4 )));
				//System.out.println(""+8+t.label.func.varSize+t.label.func.paraSize+Total_temp+Parasize+4);
			}
		}
		else if (x instanceof Load_addr) {
			Load_addr t = (Load_addr) x;
			mips.add(new Sentence("\tla $t0, "+t.label.Mips()));
			mips.add(new SW(new Temp(-8), t.reg, base));
		}
		else if (x instanceof Load_byte) {     // ??
			Load_byte t = (Load_byte) x;
			mips.add(new LW(new Temp(-10), t.add, base));
			mips.add(new Sentence("\tlb $t0, " + t.offset + "($t2)"));
			mips.add(new SB(new Temp(-8), t.res, base));
		}
		else if (x instanceof Load_imm) {
			Load_imm t = (Load_imm) x;
			mips.add(new Sentence("\tli $t0, "+t.number));

			mips.add(new SW(new Temp(-8), t.res, base));
		//	System.out.println("\tsw $t0, "+t.res.Mips(base));     change
		}
		else if (x instanceof LoadB_imm) {
			LoadB_imm t = (LoadB_imm) x;
			mips.add(new Sentence("\tli $t0, "+t.number));
			mips.add(new SB(new Temp(-8), t.res, base));
		//	System.out.println("\tsb $t0, "+t.res.Mips(base));	   change
		}
		else if (x instanceof Load_word) {
			Load_word t = (Load_word) x;
			mips.add(new LW(new Temp(-10), t.add, base));
			mips.add(new Sentence("\tlw $t0, " + t.offset + "($t2)"));
			mips.add(new SW(new Temp(-8), t.res, base));
		}
		else if (x instanceof Move) {
			Move t = (Move) x;
			mips.add(new LW(new Temp(-8), t.ori, base));
	    //	System.out.println("\tlw $t0, "+t.ori.Mips(base));     change
			mips.add(new SW(new Temp(-8), t.res, base));
	//		System.out.println("\tsw $t0, "+t.res.Mips(base));
		}
		else if (x instanceof Send_param) {
			Send_param t = (Send_param) x;
			if (t.size == 1) {
				mips.add(new LB(new Temp(-8), t.reg, base));
				mips.add(new Sentence("\tsb $t0, " + (t.start) + "($sp)"));
			}
			else {
				mips.add(new LW(new Temp(-8), t.reg, base));
				mips.add(new Sentence("\tsw $t0, " + (t.start) + "($sp)"));
			}
			/*else   //not for char array
			for (int i=0;i<t.size/4;i++) {          
				LW lw0 = new LW(new Temp(-8), t.reg, base + i * 4);
				System.out.println("\tsw $t0, " + (i * 4 + t.start) + "($sp)");
			}*/
		}
		else if (x instanceof Return) {
			Return t = (Return) x;
			if (t.reg !=null) {
				mips.add(new LW(new Temp(-2), t.reg, base));
			}
			mips.add(new Sentence("\tmove $sp, $fp"));
			mips.add(new Sentence("\tlw $fp, -4($sp)"));
			mips.add(new Sentence("\tlw $ra, -8($sp)"));
			mips.add(new Sentence("\tjr $ra"));
		}
		else if (x instanceof Store_byte) {     // ??
			Store_byte t = (Store_byte) x;
			mips.add(new LW(new Temp(-10), t.add, base));
		//	System.out.println("\tlw $t2, "+t.add.Mips(base));
			mips.add(new LB(new Temp(-8), t.reg, base));
		//	System.out.println("\tlw $t0, "+t.reg.Mips(base));
			mips.add(new Sentence("\tsb $t0, "+t.offset+"($t2)"));
		}
		else if (x instanceof Store_word) {
			Store_word t = (Store_word) x;
			mips.add(new LW(new Temp(-10), t.add, base));
		//	System.out.println("\tlw $t2, "+t.add.Mips(base));
			mips.add(new LW(new Temp(-8), t.reg, base));
		//	System.out.println("\tlw $t0, "+t.reg.Mips(base));
			mips.add(new Sentence("\tsw $t0, "+t.offset+"($t2)"));
		}
		else if (x instanceof End_func) {
			mips.add(new Sentence("\tmove $sp, $fp"));
			mips.add(new Sentence("\tlw $fp, -4($sp)"));
			mips.add(new Sentence("\tlw $ra, -8($sp)"));
			mips.add(new Sentence("\tjr $ra"));
		}
		else if (x instanceof Receive_param) {
			Receive_param t = (Receive_param) x;
			if (t.size == 1) {
				mips.add(new Sentence("\tlb $t0, " + (t.start) + "($fp)"));
				mips.add(new SB(new Temp(-8), t.res, base));
			//	System.out.println("\tsb $t0, " + t.res.Mips(base));       change
			}
			else {
				mips.add(new Sentence("\tlw $t0, " + (t.start) + "($fp)"));
				mips.add(new SW(new Temp(-8), t.res, base));               
			//	System.out.println("\tsw $t0, " + t.res.Mips(base));       change
			}
		}
		else if (x instanceof Unary) {
			Unary t = (Unary) x;
			mips.add(new LW(new Temp(-8), t.reg, base));
			mips.add(new Sentence("\t"+t.oper()+" $t0, $t0"));
			mips.add(new SW(new Temp(-8), t.res, base));
		}
		else if (x instanceof Address) {
			Address t = (Address) x;
			mips.add(new LW(new Temp(-10), new Temp(-30), 0));
			mips.add(new Sentence("\tsub $t2, $t2, " + t.ori.offset(base)));
			mips.add(new SW(new Temp(-10), t.res, base));
		}
		else if (x instanceof Gap) {
			
		}
		else {
			mips.add(new Sentence("WHAT?"));
		}
	}

	private void translate(Document x) {
		if (x instanceof _Label)
			System.out.println(x);
		else
			System.out.println("\t"+x);
	}
	

}
