package appetizer.intermediate;

import java.lang.StringBuffer;
import java.util.LinkedList;

public class Word implements Document {
	public LinkedList<Integer> list;

	public Word(LinkedList<Integer> l) 	{
		list = l;
	}
	
	public String toString() {
		boolean flag = false;
		StringBuffer buffer = new StringBuffer();
		buffer.append(".word ");
		for(Integer y: list) {
			if (flag) buffer.append(", ");
			buffer.append(y.intValue());
			flag = true;
		}
		return buffer.toString();
	}
}
