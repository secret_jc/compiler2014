package appetizer.intermediate;

import appetizer.intermediate.Comp.Op;

public class Comp_ch implements Quad
{
	public Op op;
	public Temp reg1, reg2;
	public Temp res;

	public Comp_ch(Op o, Temp r, Temp r1, Temp r2)
	{
		res = r;
		reg1 = r1;
		reg2 = r2;
		op = o;
	}
	
	public static enum Op
	{
		EQ, NE, LT, GT, LE, GE
	}	
	
	public String toString()
	{
		return res+" = "+reg1+" "+Mark()+" "+reg2;
	}
	private String Mark() {
		switch (op) {
		case EQ:
			return "==";
		case GE:
			return ">=";
		case GT:
			return ">";
		case LE:
			return "<=";
		case LT:
			return "<";
		case NE:
			return "!=";
		default:
			return "?";
		}
	}
	public String oper() {
		switch (op) {
		case EQ:
			return "seq";
		case GE:
			return "sge";
		case GT:
			return "sgt";
		case LE:
			return "sle";
		case LT:
			return "slt";
		case NE:
			return "sne";
		default:
			return "?";
		}
	}
}
