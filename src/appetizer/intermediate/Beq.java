package appetizer.intermediate;

public class Beq implements Quad 
{
	public Temp reg1, reg2;
	public Label label;

	public Beq(Temp r, Temp r2, Label l)
	{
		reg1 = r;
		reg2 = r2;
		label = l;
	}
	public String toString()
	{
		return "beq "+reg1+", "+reg2+" goto "+label;
	}
}
