package appetizer.intermediate;

public class Jump implements Quad
{
	public Label label;

	public Jump(Label l)
	{
		label = l;
	}
	public String toString()
	{
		return "j "+label;
	}
	public String Mips() {
		return "\tj "+label;
	}
}
