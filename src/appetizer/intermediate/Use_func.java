package appetizer.intermediate;

public class Use_func implements Quad 
{
	public Temp res;
	public String name;

	public Use_func(Temp r, String n)
	{
		res = r;
		name = n;
	}
	public Use_func(String n)
	{
		res = null;
		name = n;
	}
	public String toString()
	{
		if(res==null)
		{
			return name+"()";
		}
		else
		{
			return res+" = "+name+"()";
		}
	}
}
