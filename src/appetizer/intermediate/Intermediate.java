package appetizer.intermediate;

import java.util.LinkedList;
import appetizer.functions.*;

public class Intermediate {

	public LinkedList<Document> documents;
	public LinkedList<Quad> list;
	public int MaxParaSize = 0;
	public int count = 0;
	public boolean expr = false;
	public int malloc = 0;

	public Intermediate() {
		list = new LinkedList<Quad>();
		documents = new LinkedList<Document>();
	}
	
	public void add(Quad x) {
		list.add(x);
	}
	
	public void add(Document x) {
		documents.add(x);
	}
	
	public void print() {
		System.out.println("\t.data");
		for(Document x: documents) print(x);
		System.out.println("\t.text");
		System.out.println("\t.align 2");
		System.out.println("\t.globl main");
		for (Quad x: list) 
			print(x);
		System.out.println(new Printf());
	}

	private void print(Document x) {
		if(x instanceof _Label)
			System.out.println(x);
		else
			System.out.println("\t"+x);
	}
	
	private void print(Quad x) {
		if (x instanceof Label_quad)
			System.out.println(x);
		else
			System.out.println("\t"+x);
	}
}
