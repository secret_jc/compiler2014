package appetizer.intermediate;

public class Store_word implements Quad {
	public Temp reg, add;
	public int offset;

	public Store_word(Temp r, Temp a, int o)
	{
		reg = r;
		add = a;
		offset = o;
	}
	public String toString()
	{
		return "sw "+reg+", "+offset+"("+add+")";
	}
}
