package appetizer.intermediate;

public class Label_quad implements Quad 
{
	public Label label;

	public Label_quad(Label l)
	{
		label = l;
	}
	public String toString()
	{
		return label+":";
	}
	public String Mips() {
		if (label.func == null)
			return label+":";
		else {
			if (label.name.intern() == "main".intern())
				return label+":";
			else
				return "Function_"+label+":";
		}
	}
}
