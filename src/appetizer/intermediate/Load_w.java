package appetizer.intermediate;

import appetizer.intermediate.Temp;

public class Load_w implements Quad{
	public Temp t1, t2;
	
	public Load_w(Temp a1, Temp a2) {
		t1 = a1;
		t2 = a2;
	}
	
	public String toString() {
		return "\tlw " + t1 + ", " + t2;
	}
}
