package appetizer.intermediate;

public class LoadB_imm implements Quad
{
	public Temp res;
	public int number;

	public LoadB_imm(Temp r, int n)
	{
		number = n;
		res = r;
	}
	public String toString()
	{
		return res+" = "+number;
	}
}
