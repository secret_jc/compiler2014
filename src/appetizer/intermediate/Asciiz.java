package appetizer.intermediate;

import java.lang.StringBuffer;

public class Asciiz implements Document
{
	public String str;

	public Asciiz(String s)
	{
		str = s;
	}
	public String toString()
	{
		StringBuffer res = new StringBuffer();
		res.append(".asciiz \"");
		for(int i=0;i<str.length();++i)
		{
			char c = str.charAt(i);
			switch(c)
			{
				case '\n':
					res.append("\\n");
					break;
				case '\t':
					res.append("\\t");
					break;
				case '\r':
					res.append("\\r");
					break;
				default:
					res.append(c);
			}
		}
		res.append('\"');
		return res.toString();
	}
}
