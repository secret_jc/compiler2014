package appetizer.intermediate;

public class Load_byte implements Quad
{
	public Temp res, add;
	public int offset;
	
	public Load_byte(Temp r, Temp a, int o)
	{
		res = r;
		add = a;
		offset = o;
	}
	public String toString()
	{
		return "lb "+res+", "+offset+"("+add+")";
	}
}
