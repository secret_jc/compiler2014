package appetizer.intermediate;

public class Malloc implements Quad {
	
	public Temp add;
	public Temp tmp;

	public Malloc(Temp a, Temp t) {
		add = a;
		tmp = t;
	}
	
	public String toString() {
		return "malloc " + add + " " + tmp;
	}

}
