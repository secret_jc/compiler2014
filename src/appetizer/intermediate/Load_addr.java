package appetizer.intermediate;

public class Load_addr implements Quad 
{
	public Temp reg;
	public Label label;

	public Load_addr(Temp r, Label l)
	{
		reg = r;
		label = l;
	}
	public String toString()
	{
		return "la "+reg+", "+label;
	}
}
