package appetizer.intermediate;

public class Bnez implements Quad 
{
	public Temp reg;
	public Label label;

	public Bnez(Temp r, Label l)
	{
		reg = r;
		label = l;
	}
	public String toString()
	{
		return "bnez "+reg+" goto "+label;
	}
}
