package appetizer.intermediate;

import appetizer.ast.*;

public class Binary implements Quad 
{
	public Temp left;
	public Temp right;
	public Temp res;
	public Double_expression.Op op;

	public Binary(Double_expression.Op o, Temp r, Temp l, Temp ri)
	{
		left = l;
		right = ri;
		res = r;
		op = o;
	}
	public String toString()
	{
		return res + " = " + left +" "+ Double_expression.getOp(op) +" "+ right;
	}
	public String oper() {
		switch (op) {
		case DIVIDE:
			return "div";  
		case MINUS:
			return "sub";
		case PLUS:
			return "add";
		case TIMES:
			return "mul";
		case MODULE:
			return "rem";
		case BIT_OR:
			return "or";
		case BIT_XOR:
			return "xor";
		case BIT_AND:
			return "and";
		case SHL:
			return "sll";  
		case SHR:
			return "sra";
		default:
			return "what Binary?";
		}
	}
}
