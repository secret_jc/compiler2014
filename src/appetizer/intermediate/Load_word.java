package appetizer.intermediate;

public class Load_word implements Quad 
{
	public Temp res, add;
	public int offset;
	
	public Load_word(Temp r, Temp a, int o)
	{
		res = r;
		add = a;
		offset = o;
	}
	public String toString()
	{
		return "lw "+res+", "+offset+"("+add+")";
	}
}
