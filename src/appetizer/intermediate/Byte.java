package appetizer.intermediate;

import java.lang.StringBuffer;
import java.util.LinkedList;

public class Byte implements Document 
{
	public LinkedList<Character> list;

	public Byte(LinkedList<Character> l)
	{
		list = l;
	}
	public String toString()
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append(".byte ");
		int f = 0;
		for(Character y: list)
		{
			if(f==1) buffer.append(", ");
			buffer.append(y.charValue());
			f = 1;
		}
		return buffer.toString();
	}
}
