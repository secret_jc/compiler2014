package appetizer.intermediate;

import appetizer.ast.*;

public class Unary implements Quad
{
	public Unary_expression.Op op;
	public Temp reg, res;

	public Unary(Unary_expression.Op o, Temp r, Temp l)
	{
		op = o;
		reg = l;
		res = r;
	}
	public String toString()
	{
		return res+" = "+Unary_expression.getOp(op)+" "+reg;
	}
	public String oper() {
		switch (op) {
		case NEGATIVE:
			return "neg";  
		case BIT_NOT:
			return "not";
		default:
			return "what Binary?";
		}
	}
}
