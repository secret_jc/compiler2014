package appetizer.intermediate;

import appetizer.semantics.FuncEntry;

public class Label
{
	private static int count = 0;
	public String name;

	public FuncEntry func;	//record the beginning of a function

	int i;

	public Label()
	{
		i = ++count;
		name = "L"+i;
		func = null;
	}

	public Label(String n, FuncEntry entry)
	{
		name = n;
		func = entry;
	}

	public String toString() {
		return name;
	}
	
	public String Mips() {
		return name;
	}
}
