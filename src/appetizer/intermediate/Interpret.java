package appetizer.intermediate;

import appetizer.ast.*;
import appetizer.semantics.*;
import appetizer.functions.*;

import java.util.Stack;
import java.util.LinkedList;
import java.util.Hashtable;

public class Interpret {
	
	private Program program;
	private Intermediate Inter;  // all the information
	private Stack<Label> Label_B, Label_C;   //For loop
	private FuncEntry Func_now;	//function now
	private Temp fp, sp, ze;  //common registers
	private Hashtable<String, Label> String_table;	//string-label
	private Quad Quad1=null, Quad2=null, Quad3=null;
	private int Num_argument = 0;
	private Declaration bak;
	private boolean Flag;

	public Interpret(Object x, boolean F) {
		program = (Program) x;
		sp = new Temp("$sp",-1);
		fp = new Temp("$fp",-2);
		ze = new Temp("$0",0);
		Label_B = new Stack<Label>();
		Label_C = new Stack<Label>();
		Inter = new Intermediate();
		Flag = F;
	}
	
	public Intermediate Interpret() {
		Interpret_program(program);
		return Inter;
	}
	
	private void Interpret_program(Program x) {
		for (DeclOrDef decl: x.list) {
			if (decl instanceof Declaration) {
				Interpret_declaration_G((Declaration) decl);
			}
			else 
				Interpret_function_definition((Function_definition) decl);
		}
	}
	

	private void Interpret_function_definition(Function_definition x) {
		Num_argument = 0;
		Func_now = x.entry;
		String name = x.plaindecl.name;
		Label l = new Label(name, x.entry);
		FuncEntry entry = x.entry;
		entry.label = l;
		Inter.add(new Label_quad(l));
		if (name.intern() == "main".intern() && Quad1!=null) {
			Inter.add(Quad1);
			Inter.add(Quad2);
			Inter.add(Quad3);
		}

		if (Flag) {
			Inter.add(new Load_imm(new Temp(-5), 1));
			Inter.add(new Load_imm(new Temp(-6), 1));
			Inter.add(new Load_imm(new Temp(-7), 1));
		}
		int offset = 0;
		for (VarEntry var: entry.parameters) {        //char array
			Num_argument = Num_argument + 1;
			//if ((offset%4!=0) && !(var.type instanceof Char)) 
				//	offset = (offset/4+1)*4;
			Temp res = new Temp();
			var.loc = res;
			int g = Sizeof.sizeof(var.type); //
			if (var.type instanceof Array)
				Inter.add(new Receive_param(res, offset, 4));             //
			else
				Inter.add(new Receive_param(res, offset, Sizeof.sizeof(var.type)));
			if (g == 1)
				offset = offset + 4;
			else {
				if (var.type instanceof Array)
					offset = offset + 4;             //
				else
					offset = offset + Sizeof.sizeof(var.type);
			}
		}
		Interpret_compound_statement(x.compstmt);
		entry.tempSize = Temp.count * 4;    
		Inter.add(new End_func());
		Inter.add(new Gap());
		if (entry.paraSize > Inter.MaxParaSize)
			Inter.MaxParaSize = entry.paraSize;
		if (Temp.count>2000) Inter.expr = true;   //
		Temp.count = 0;  
		Func_now = null;
	}
	
	private	void Interpret_declaration_G(Declaration x) 	{
		if (x.init_declarators==null) return;
		Label label;
		for (Init_declarator y: x.init_declarators.init_declarator_list) {
			VarEntry entry = y.declarator.entry;
			if(y.initializer!=null) {
				Initializer init = y.initializer;
				if(init.expr==null)	{ //array initializer
					label = new Label();
					Inter.add(new _Label(label));
					LinkedList<Initializer> list = init.initializer_list.initializer_list;
					LinkedList<Integer> res = new LinkedList<Integer>();
					int size = Sizeof.sizeof(entry.type);
					for (Initializer initializer: list) {
						res.add(initializer.expr.result);
						size = size - 4;
					}
					Inter.add(new Word(res));
					Inter.add(new Space(size));
					entry.label = label;
				}
				else {	//expr initializer
					if (entry.type instanceof Pointer){
						Primary_expression xx= (Primary_expression) init.expr;
						String str = (String) xx.obj;
						if (String_table==null) String_table = new Hashtable<String, Label>();
						Label l = String_table.get(str.intern());
						if(l==null) 
						{
							l = new Label();
							Inter.add(new _Label(l));
							Inter.add(new Asciiz(str));
							String_table.put(str, l);
						}
						
						Temp t0 = new Temp(-11);
						LinkedList<Integer> list = new LinkedList<Integer>();
						list.add(new Integer(0));
						label = new Label();
						Inter.add(new _Label(label));
						Inter.add(new Word(list));
						Quad1 = new Load_addr(t0, l);
						Temp t1 = new Temp(-12);
						Quad2 = new Load_addr(t1, label);
						Quad3 = new Store_word(t0, t1, 0);
					}
					else {
						label = new Label();
						Inter.add(new _Label(label));
						if (entry.type instanceof Char) {
							LinkedList<Character> list = new LinkedList<Character>();
							list.add(new Character((char) init.expr.result));
							Inter.add(new Byte(list));
						}
						else
						{
							LinkedList<Integer> list = new LinkedList<Integer>();
							list.add(new Integer(init.expr.result));
							Inter.add(new Word(list));
						}
					}
					entry.label = label;
				}
			}
			else {
				label = new Label();
				Inter.add(new _Label(label));
				Inter.add(new Space(Sizeof.sizeof(entry.type)));
				entry.label = label;
			}
		}
	}
	
	private void Interpret_declaration_L(Declaration x) {
		if(x.init_declarators==null) return;
		for(Init_declarator y: x.init_declarators.init_declarator_list) {
			VarEntry entry = y.declarator.entry;
			if (y.initializer==null) {
				/*  VarEntry var = (VarEntry) entry;
				    if (entry.type instanceof Array) {    //set address for array
					Temp reg = new Temp();
					Temp start = address_identifier(var);
					Inter.add(new Move(reg, start));
					var.loc = reg;
				}*/
			}
			else {
				Initializer init = y.initializer;
				if(init.expr==null)	{ //Array
					LinkedList<Initializer> list = init.initializer_list.initializer_list;
					Array arrayType = (Array) entry.type;
					Temp start = address_identifier(y.declarator.entry);  //Set entries -> 0
					int size = Sizeof.sizeof(arrayType);
					memset(start, size, 0);					
					for(Initializer elem: list) {  //Set common
						Temp tmp = Interpret_all_expression(elem.expr);
						Inter.add(new Store_word(tmp, start, 0));
						Inter.add(new Binary_Imm(Double_expression.Op.PLUS, start, start, 4));
					}
				}
				else {	//Expression
					VarEntry var = (VarEntry) entry;
					if(entry.type instanceof Record) {	//struct
						Expr expr = init.expr;
						Temp add = new Temp();
						Inter.add(new Binary_Imm(Double_expression.Op.MINUS, add, fp, Func_now.varSize + Sizeof.init_func() - var.offset));
						if((expr instanceof Postfix_expression)&&(((Postfix_expression) expr).op==Postfix_expression.Op.PAREN)) {	//function return a record
							Postfix_expression expr2 = (Postfix_expression) expr;
							int paraSize = Interpret_arguments((Arguments) expr2.obj);
							Inter.add(new Send_param(add, paraSize, Sizeof.sizeof(new Pointer())));	//directly write the result at address add
							Inter.add(new Use_func(expr2.expr.toString()));
						}
						else {
							Temp right_add = address_all_expression(expr);
							int size = Sizeof.sizeof(entry.type);
							memcpy(add, right_add, size);
						}
					}
					else {
						if(init.expr.isConst) {
							Temp reg = new Temp();
							Inter.add(new Load_imm(reg, init.expr.result));
							var.loc = reg;
						}
						else if (init.expr instanceof Primary_expression) {
							Temp tmp = Interpret_all_expression(init.expr);
							Temp res = new Temp();
							Inter.add(new Move(res, tmp));
							var.loc = res;
						}
						else {
							Temp res = Interpret_all_expression(init.expr);
							var.loc = res;
						}
					}
				}
			}
		}
	}
	
	private void Interpret_all_statement(Statement x) {
		if(x==null) return;
		else if(x instanceof Compound_statement) 
		{
			Compound_statement y = (Compound_statement) x;
			Interpret_compound_statement(y);
		}
		else if(x instanceof Expression_statement) 
		{
			Expression_statement y = (Expression_statement) x;
			Interpret_all_expression(y.expr);
		}
		else if(x instanceof Iteration_statement)
		{
			Iteration_statement y = (Iteration_statement) x;
			Interpret_iteration_statement(y);
		}
		else if(x instanceof Selection_statement)
		{
			Selection_statement y = (Selection_statement) x;
			Interpret_selection_statement(y);
		}
		else
		{
			Jump_statement y = (Jump_statement) x;
			Interpret_jump_statement(y);
		}
	}
	
	private void Interpret_compound_statement(Compound_statement x) 	{
		for(Declaration y: x.declaration_list.declarations)
			Interpret_declaration_L(y);
		for(Statement y: x.stmt_list.stmts)
			Interpret_all_statement(y);
	}
	
	private void Interpret_selection_statement(Selection_statement x) {
		if (x.elsestmt==null) {  // without else
			Label end = new Label();
			boolean g = false;
			if (x.expr instanceof Double_expression) {
				Double_expression t = (Double_expression) x.expr;
				if (t.op == Double_expression.Op.AND) {
					g = true;
					Temp reg1 = Interpret_all_expression(t.left);
					Inter.add(new Beqz(reg1, end));
					Temp reg2 = Interpret_all_expression(t.right);
					Inter.add(new Beqz(reg2, end));
					Interpret_all_statement(x.thenstmt);
					Inter.add(new Label_quad(end));
				}
			}
			if (!g) {
				Temp reg = Interpret_all_expression(x.expr);
				Inter.add(new Beqz(reg, end));
				Interpret_all_statement(x.thenstmt);
				Inter.add(new Label_quad(end));
			}
		}
		else { // else 
			Temp reg = Interpret_all_expression(x.expr);
			Label lb = new Label();
			Inter.add(new Beqz(reg, lb));
			Interpret_all_statement(x.thenstmt);
			Label end = new Label();
			Inter.add(new Jump(end));
			Inter.add(new Label_quad(lb));
			Interpret_all_statement(x.elsestmt);
			Inter.add(new Label_quad(end));
		}
	}
	
	private void Interpret_iteration_statement(Iteration_statement x) {
		switch(x.type) {
			case While:	
				Label begin = new Label();
				Label end = new Label();
				Label_B.push(end);
				Label_C.push(begin);
				Inter.add(new Label_quad(begin));
				Temp reg = Interpret_all_expression(x.expr1);
				Inter.add(new Beqz(reg, end));
				Interpret_all_statement(x.stmt);
				Inter.add(new Jump(begin));
				Inter.add(new Label_quad(end));
				Label_B.pop();
				Label_C.pop();
				break;
			default:	//For
				begin = new Label();
				end = new Label();
				Label middle = new Label();
				Label_B.push(end);
				Label_C.push(middle);
				Interpret_all_expression(x.expr1);
				Inter.add(new Label_quad(begin));
				reg = Interpret_all_expression(x.expr2);
				Inter.add(new Beqz(reg, end));
				Interpret_all_statement(x.stmt);
				Inter.add(new Label_quad(middle));
				Interpret_all_expression(x.expr3);
				Inter.add(new Jump(begin));
				Inter.add(new Label_quad(end));
				Label_B.pop();
				Label_C.pop();
		}
	}
	private void Interpret_jump_statement(Jump_statement x)
	{
		switch(x.type)
		{
			case Return:
				Function functionType = (Function) Func_now.type;
				if(functionType.returnType instanceof Record)	//return a struct, to copy the result into its destination address
				{
			/*		Temp add = new Temp();
					Inter.add(new Load_word(add, fp, Func_now.paraSize - Sizeof.sizeof(new Pointer())));
					Temp add_ori = address_all_expression(x.expr);
					int size = Sizeof.sizeof(x.expr.entry.type);
					memcpy(add, add_ori, size);	*/
					Temp res = Interpret_all_expression(x.expr);
					Inter.add(new Return(res));
				}
				else
				{
					Temp res = Interpret_all_expression(x.expr);
					Inter.add(new Return(res));
				}
				break;
			case Continue:
				Inter.add(new Jump(Label_C.peek()));
				break;
			default: //Break
				Inter.add(new Jump(Label_B.peek()));
				break;
		}
	}
	
	private Temp Interpret_all_expression(Expr x) {
		if(x==null) return null;
		else if(x.isConst) 
		{
			if(x.result == 0) return ze;
			Temp res = new Temp();
			//Temp res = new Temp();
			Inter.add(new Load_imm(res, x.result));
			return res;
		}
		else if(x instanceof Expression)
		{
			return Interpret_all_expressionession((Expression) x);	
		}
		else if(x instanceof Assignment_expression) { 
			Assignment_expression y = (Assignment_expression) x;
			if (y.left.entry.type instanceof Record) { //struct    
				if (y.left.entry.type instanceof Record) { //struct    
					if (y.right instanceof Postfix_expression) {
						Postfix_expression t = (Postfix_expression) y.right;
						if (t.op != Postfix_expression.Op.PAREN)
							return address_assignment_expression(y);
					}
					else return address_assignment_expression(y);
				}
				Temp tmp1 = Interpret_all_expression(y.right);
				Temp tmp2 = address_all_expression(y.left);
				Temp tmp3 = new Temp(-9);
				Temp tmp4 = new Temp(-11);
				Temp tmp5 = new Temp(-12);
				Inter.add(new Move(tmp3, tmp1));
				Inter.add(new Move(tmp4, tmp2));
				Record record = (Record) y.left.entry.type;
				for (int i=0;i<record.size;i++) {
					Inter.add(new Load_byte(tmp5, tmp3, 0));
					Inter.add(new Store_byte(tmp5, tmp4, 0));
					Inter.add(new Binary_Imm(Double_expression.Op.PLUS, tmp4, tmp4, 1));
					Inter.add(new Binary_Imm(Double_expression.Op.PLUS, tmp3, tmp3, 1));
				}
				return tmp2;    /*wonly address*/
				//return address_all_expression(y.left);
				//return address_assignment_expression(y);
			}
			else { //other than struct
				return Interpret_assignment_expression(y);
			}
		}
		else if(x instanceof Double_expression) {
			return Interpret_double_expression((Double_expression) x);	
		}
		else if(x instanceof Cast_expression) {
			return Interpret_cast_expression((Cast_expression) x);	
		}
		else if(x instanceof Postfix_expression) {
			return Interpret_postfix_expression((Postfix_expression) x);	
		}
		else if (x instanceof Unary_expression) {
			return Interpret_unary_expression((Unary_expression) x);	
		}
		else {
			return Interpret_primary_expression((Primary_expression) x);	
		}
	}
	
	private Temp Interpret_all_expressionession(Expression x) {
		Interpret_all_expression(x.left);
		return Interpret_all_expression(x.right);
	}
	
	private Temp Interpret_assignment_expression(Assignment_expression x) {
		Expr left = x.left, right = x.right;
		if (Flag || (left instanceof Primary_expression)&&((VarEntry) left.entry).function!=null) {  //local var using
			if(right.isConst)
			{
				Primary_expression y = (Primary_expression) left;
				VarEntry entry = (VarEntry) y.entry;
				Temp res = entry.getLoc();
				switch(x.op)
				{
					case ASSIGN:
						Inter.add(new Load_imm(res, right.result));
						return res;
					default: //others
						Inter.add(new Binary_Imm(Assignment_expression.Get_BinaryOp(x.op), res, res, right.result));
						return res;
				}
			}
			else {
				Temp reg = Interpret_all_expression(right);
				Primary_expression y = (Primary_expression) left;
				VarEntry entry = (VarEntry) y.entry;
				Temp res = entry.getLoc();
				switch(x.op) {
					case ASSIGN:
						Inter.add(new Move(res, reg));
						return res;
					default: //others
						Inter.add(new Binary(Assignment_expression.Get_BinaryOp(x.op), res, res, reg));
						return res;
				}
			}
		}
		else if(left instanceof Primary_expression)	//global variable
		{
			Primary_expression y = (Primary_expression) left;
			VarEntry entry = (VarEntry) y.entry;
			Temp add = address_all_expression(left);
			Temp res;
			if(right.isConst) {
				switch(x.op) {
					case ASSIGN:
						if(entry.type instanceof Char) {
							res = new Temp();
							Inter.add(new LoadB_imm(res, right.result));
							Inter.add(new Store_byte(res, add, 0));
						}
						else {
							res = new Temp();
							Inter.add(new Load_imm(res, right.result));
							Inter.add(new Store_word(res, add, 0));
						}
						return res;
					default: //others
						res = Interpret_all_expression(left);
						Inter.add(new Binary_Imm(Assignment_expression.Get_BinaryOp(x.op), res, res, right.result));
						if(entry.type instanceof Char) Inter.add(new Store_byte(res, add, 0));
						else Inter.add(new Store_word(res, add, 0));
						return res;
				}
			}
			else {
				Temp reg = Interpret_all_expression(right);
				switch(x.op) {
					case ASSIGN:
						if(entry.type instanceof Char) Inter.add(new Store_byte(reg, add, 0));
						else Inter.add(new Store_word(reg, add, 0));
						return reg;
					default: //others
						res = Interpret_all_expression(left);
						Inter.add(new Binary(Assignment_expression.Get_BinaryOp(x.op), res, res, reg));
						if(entry.type instanceof Char) Inter.add(new Store_byte(res, add, 0));
						else Inter.add(new Store_word(res, add, 0));
						return res;
				}
			}
		}
		else if(left instanceof Postfix_expression) {
			Postfix_expression y = (Postfix_expression) left;
			Temp add = address_postfix_expression(y);
			Temp res;
			if(right.isConst) {
				switch(x.op) {
					case ASSIGN:
						if(left.entry.type instanceof Char) {
							res = new Temp();
							Inter.add(new LoadB_imm(res, right.result));
							Inter.add(new Store_byte(res, add, 0));
						}
						else {
							res = new Temp();
							Inter.add(new Load_imm(res, right.result));
							Inter.add(new Store_word(res, add, 0));
						}
						return res;
					default: //others
						res = Interpret_postfix_expression(y);
						Inter.add(new Binary_Imm(Assignment_expression.Get_BinaryOp(x.op), res, res, right.result));
						if(left.entry.type instanceof Char)
						{
							Inter.add(new Store_byte(res, add, 0));
						}
						else
						{
							Inter.add(new Store_word(res, add, 0));
						}
						return res;
				}
			}
			else
			{
				switch(x.op)
				{
					case ASSIGN:
						Temp right_res = Interpret_all_expression(right);
						if(left.entry.type instanceof Char)
						{
							Inter.add(new Store_byte(right_res, add, 0));
						}
						else
						{
							Inter.add(new Store_word(right_res, add, 0));
						}
						return right_res;
					default: //others
						right_res = Interpret_all_expression(right);
						res = Interpret_postfix_expression(y);
						Inter.add(new Binary(Assignment_expression.Get_BinaryOp(x.op), res, res, right_res));
						if(left.entry.type instanceof Char)
						{
							Inter.add(new Store_byte(res, add, 0));
						}
						else
						{
							Inter.add(new Store_word(res, add, 0));
						}
						return res;
				}		

			}
		}
		else	//Unary_expression
		{
			//must be STAR
			Temp add = address_all_expression(left);
			if(right.isConst)
			{
				Temp res = new Temp();
				Inter.add(new Load_imm(res, right.result));
				if(left.entry.type instanceof Char)
				{
					Inter.add(new Store_byte(res, add, 0));
				}
				else
				{
					Inter.add(new Store_word(res, add, 0));
				}
				return res;
			}
			else
			{
				Temp res = Interpret_all_expression(right);
				if(left.entry.type instanceof Char)
				{
					Inter.add(new Store_byte(res, add, 0));
				}
				else
				{
					Inter.add(new Store_word(res, add, 0));
				}
				return res;
			}
		}
	}
	private Temp Interpret_double_expression(Double_expression x) {
		Expr left = x.left, right = x.right;
		Temp res = new Temp(), tmp;
		Label l1,l2;
		switch(x.op)
		{
			case OR:   
				Inter.add(new Load_imm(res, 0));
				tmp = Interpret_all_expression(left);
				l1 = new Label(); l2 = new Label();
				Inter.add(new Bnez(tmp, l1));
				tmp = Interpret_all_expression(right);
				Inter.add(new Bnez(tmp, l1));
				Inter.add(new Jump(l2));
				Inter.add(new Label_quad(l1));
				Inter.add(new Load_imm(res, 1));
				Inter.add(new Label_quad(l2));
				return res;
			case AND:
				Temp tl, tr;
				
				Inter.add(new Load_imm(res, 0));
				tmp = Interpret_all_expression(left);
				l2 = new Label();
				Inter.add(new Beqz(tmp, l2));
				tmp = Interpret_all_expression(right);
				Inter.add(new Beqz(tmp, l2));
				Inter.add(new Load_imm(res, 1));
				Inter.add(new Label_quad(l2));
				return res;
				/*
				tl = Interpret_all_expression(left);
				tr = Interpret_all_expression(right);
				Inter.add(new Comp(Comp.Op.EQ, res, tl, tr));
				Inter.add(new Load_imm(res, 1));
				tmp = Interpret_all_expression(left);
				l1 = new Label(); l2 = new Label();
				Inter.add(new Beqz(tmp, l1));
				tmp = Interpret_all_expression(right);
				Inter.add(new Beqz(tmp, l1));
				Inter.add(new Jump(l2));
				Inter.add(new Label_quad(l1));
				Inter.add(new Load_imm(res, 0));
				Inter.add(new Label_quad(l2));
				return res;*/
			case EQ:	
				tl = Interpret_all_expression(left);
				tr = Interpret_all_expression(right);
				Inter.add(new Comp(Comp.Op.EQ, res, tl, tr));
				return res;
			case NE:
				tl = Interpret_all_expression(left);
				tr = Interpret_all_expression(right);
				if (left.entry.type instanceof Char) 
					Inter.add(new Comp_ch(Comp_ch.Op.NE, res, tl, tr));
				else
					Inter.add(new Comp(Comp.Op.NE, res, tl, tr));
				return res;
			case LT:
				tl = Interpret_all_expression(left);
				tr = Interpret_all_expression(right);
				Inter.add(new Comp(Comp.Op.LT, res, tl, tr));
				return res;
			case GT:
				tl = Interpret_all_expression(left);
				tr = Interpret_all_expression(right);
				Inter.add(new Comp(Comp.Op.GT, res, tl, tr));
				return res;
			case LE:
				tl = Interpret_all_expression(left);
				tr = Interpret_all_expression(right);
				Inter.add(new Comp(Comp.Op.LE, res, tl, tr));
				return res;
			case GE:
				tl = Interpret_all_expression(left);
				tr = Interpret_all_expression(right);
				Inter.add(new Comp(Comp.Op.GE, res, tl, tr));
				return res;
			default:	//correspond with MIPS instructions
				if(left.isConst)
				{
					Expr tmpp = x.left;
					x.left = x.right;
					x.right = tmpp;
				}
				if(right.isConst)
				{
					Temp l = Interpret_all_expression(left);
					int number = right.result;
					if (left.entry.type instanceof Array) {
						Array array = (Array)left.entry.type;
						number = number * Sizeof.sizeof(array.elementType);
					}

					if (left.entry.type instanceof Pointer) {
						Pointer pointer = (Pointer)left.entry.type;
						number = number * Sizeof.sizeof(pointer.elementType);
					}
					Inter.add(new Binary_Imm(x.op, res, l, number));
					return res;
				}
				else
				{
					Temp l = Interpret_all_expression(left);
					Temp r = Interpret_all_expression(right);
					if (left.entry.type instanceof Array) {
						Array array = (Array)left.entry.type;
						Inter.add(new Binary_Imm(Double_expression.Op.TIMES, r, r, Sizeof.sizeof(array.elementType)));
					}
				

					if (left.entry.type instanceof Pointer) {
						Pointer pointer = (Pointer)left.entry.type;
						Inter.add(new Binary_Imm(Double_expression.Op.TIMES, r, r, Sizeof.sizeof(pointer.elementType)));
					}
					Inter.add(new Binary(x.op, res, l, r));
					return res;
				}
				
		}
	}
	private Temp Interpret_unary_expression(Unary_expression x)
	{
		switch(x.op)
		{
			case INC:
				if((x.expr instanceof Primary_expression)&&((VarEntry) x.expr.entry).function!=null)
				{
					VarEntry entry = (VarEntry) x.expr.entry;
					Temp res = entry.getLoc();
					Inter.add(new Binary_Imm(Double_expression.Op.PLUS, res, res, 1));
					return res;
				}
				else	//Postfix_expression or Unary_expression or global variable
				{
					Temp res = Interpret_all_expression(x.expr);
					Temp add = address_all_expression(x.expr);
					Inter.add(new Binary_Imm(Double_expression.Op.PLUS, res, res, 1));
					if(x.expr.entry.type instanceof Char)
					{
						Inter.add(new Store_byte(res, add, 0));
					}
					else
					{
						Inter.add(new Store_word(res, add, 0));
					}
					return res;
				}
			case DEC:
				if(x.expr instanceof Primary_expression&&((VarEntry) x.expr.entry).function!=null)
				{
					VarEntry entry = (VarEntry) x.expr.entry;
					Temp res = entry.getLoc();
					Inter.add(new Binary_Imm(Double_expression.Op.MINUS, res, res, 1));
					return res;
				}
				else	//Postfix_expression or Unary_expression
				{
					Temp res = Interpret_all_expression(x.expr);
					Temp add = address_all_expression(x.expr);
					Inter.add(new Binary_Imm(Double_expression.Op.MINUS, res, res, 1));
					if(x.expr.entry.type instanceof Char)
					{
						Inter.add(new Store_byte(res, add, 0));
					}
					else
					{
						Inter.add(new Store_word(res, add, 0));
					}
					return res;
				}
			case SIZEOF:
				Temp res = new Temp();
				Inter.add(new Load_imm(res, x.result));      /**/
				return res;
			case BIT_AND:                    /**/
				//Temp add = Interpret_all_expression(x.expr);
				//res = new Temp();
				//Inter.add(new Address(res, add));
				return address_all_expression(x.expr);
			case STAR:	//if p is a struct, you can do nothing to take *p here. Anything make sense will not call this method.
				Temp tmp = Interpret_all_expression(x.expr);
				res = new Temp();
				Type tmptype = x.expr.entry.type;
				if(tmptype instanceof Pointer)
				{
					Pointer pointer = (Pointer) tmptype;
					if(pointer.elementType instanceof Char)
					{
						Inter.add(new Load_byte(res, tmp, 0));
					}
					else
					{
						Inter.add(new Load_word(res, tmp, 0));
					}
				}
				else	//Array
				{
					Array pointer = (Array) tmptype;
					if(pointer.elementType instanceof Char)
					{
						Inter.add(new Load_byte(res, tmp, 0));
					}
					else
					{
						Inter.add(new Load_word(res, tmp, 0));
					}
				}
				return res;
			case POSITIVE:
				return Interpret_all_expression(x.expr);
			case NEGATIVE:
				res = new Temp();
				tmp = Interpret_all_expression(x.expr);
				Inter.add(new Unary(x.op, res, tmp));
				return res;
			case BIT_NOT:          /**/
				tmp = Interpret_all_expression(x.expr);
				res = new Temp();
				Inter.add(new Unary(x.op, res, tmp));
				return res;
			default: //NOT         /**/
				res = new Temp();
				Inter.add(new Load_imm(res, 0));
				tmp = Interpret_all_expression(x.expr);
				Label l1 = new Label();
				Label l2 = new Label();
				Inter.add(new Beqz(tmp, l1));
				Inter.add(new Jump(l2));
				Inter.add(new Label_quad(l1));
				Inter.add(new Load_imm(res, 1));
				Inter.add(new Label_quad(l2));
				return res;
		}
	}
	
	private Temp Interpret_primary_expression(Primary_expression x) {
		if(x.isConst) {
			Temp res = new Temp();
			Inter.add(new Load_imm(res, x.result));
			return res;
		}
		switch(x.op) {
			case ID:
				VarEntry entry = (VarEntry) x.entry;
				Temp res;
				if (entry.function == null && (!Flag)) {
				//if (entry.function == null) {
				//if (entry.function==null&&Func_now!=entry.locFunc)	//global first generation in this function
					res = new Temp();
					Temp add = address_all_expression(x);
					if(entry.type instanceof Char) {
						Inter.add(new Load_byte(res, add, 0));
					}
					else {
						Inter.add(new Load_word(res, add, 0));
					}
					entry.loc = res;
					entry.locFunc = Func_now;
					return res;
				}
				else { 
					res = entry.getLoc();
					if (entry.type instanceof Array || entry.type instanceof Record) {          //                  
						Inter.add(new Move(res, address_all_expression(x))); 
					}
					return res;
				}
			default: //STRING
				String str = (String) x.obj;
				if (String_table==null) String_table = new Hashtable<String, Label>();
				Label l = String_table.get(str.intern());
				Temp add = new Temp();
				if(l==null) 
				{
					l = new Label();
					Inter.add(new _Label(l));
					Inter.add(new Asciiz(str));
					String_table.put(str, l);
				}
				Inter.add(new Load_addr(add, l));
				return add;
		}
	}
	
	private Temp Interpret_cast_expression(Cast_expression x) {
		Temp res = new Temp();
		Temp tmp = Interpret_all_expression(x.expr);
		Inter.add(new Move(res, tmp));
		return res;
	}
	
	private Temp Interpret_postfix_expression(Postfix_expression x)
	{
		switch(x.op)
		{
			case PAREN:	//function call, return the address when returning a record
				{
					if (x.expr.toString().intern() == "malloc".intern()) {
						Inter.malloc = Inter.malloc + 1;
						Arguments argu = (Arguments) x.obj;
						Temp tmp;
						Temp add = null;
						for (Expr expr: argu.exprs) {
							tmp = Interpret_all_expression(expr);
							add = new Temp();
							Inter.add(new Malloc(add, tmp));
						}
						return add;
					}
					
					Temp add = new Temp();
					if(x.entry.type instanceof Record)
					{
						int size = Sizeof.sizeof(x.entry.type);
						Inter.add(new Binary_Imm(Double_expression.Op.MINUS, add, fp, Func_now.varSize + Func_now.tempSize - Func_now.paraSize + Sizeof.init_func() + (size/4+1)*4));
						Func_now.tempSize = Func_now.tempSize + (size/4+1)*4;
					}

					int paraSize = Interpret_arguments((Arguments) x.obj);
					
				/*	if(x.entry.type instanceof Record)
					{	
						Inter.add(new Send_param(add, paraSize, Sizeof.sizeof(new Pointer())));
						Inter.add(new Use_func(x.expr.toString()));
						return add;
					}
					else
					{
						Temp res = new Temp();
						Inter.add(new Use_func(res, x.expr.toString()));
						return res;
					}*/
					
					Temp res = new Temp();
					if (x.expr.toString().intern() == "printf".intern()) 
						Inter.count = Inter.count + 1;
					if (paraSize == 4 && x.expr.toString().intern() == "printf".intern())      //printf optimize
						Inter.add(new Use_func(res, "printf2"));
					else
						Inter.add(new Use_func(res, x.expr.toString()));
					return res;
				}
			case INC:
				if(x.expr instanceof Primary_expression&&((VarEntry) x.expr.entry).function!=null)
				{
					VarEntry entry = (VarEntry) x.expr.entry;
					Temp res = new Temp();
					Temp tmp = entry.getLoc();
					Inter.add(new Move(res, tmp));
					Inter.add(new Binary_Imm(Double_expression.Op.PLUS, tmp, tmp, 1));
					return res;
				}
				else	//Postfix_expression or Unary_expression
				{
					Temp res = Interpret_all_expression(x.expr);
					Temp tmp = new Temp();
					Inter.add(new Move(tmp, res));
					Temp add = address_all_expression(x.expr);
					Inter.add(new Binary_Imm(Double_expression.Op.PLUS, tmp, tmp, 1));
					if(x.expr.entry.type instanceof Char)
					{
						Inter.add(new Store_byte(tmp, add, 0));
					}
					else
					{
						Inter.add(new Store_word(tmp, add, 0));
					}
					return res;
				}
			case DEC:
				if(x.expr instanceof Primary_expression&&((VarEntry) x.expr.entry).function!=null)
				{
					VarEntry entry = (VarEntry) x.expr.entry;
					Temp res = new Temp();
					Temp tmp = entry.getLoc();
					Inter.add(new Move(res, tmp));
					Inter.add(new Binary_Imm(Double_expression.Op.MINUS, tmp, tmp, 1));
					return res;
				}
				else	//Postfix_expression or Unary_expression
				{
					Temp res = Interpret_all_expression(x.expr);
					Temp tmp = new Temp();
					Inter.add(new Move(tmp, res));
					Temp add = address_all_expression(x.expr);
					Inter.add(new Binary_Imm(Double_expression.Op.MINUS, tmp, tmp, 1));
					if(x.expr.entry.type instanceof Char)
					{
						Inter.add(new Store_byte(tmp, add, 0));
					}
					else
					{
						Inter.add(new Store_word(tmp, add, 0));
					}
					return res;
				}
			default:	//other postfixExpr which has address
				Temp add = address_postfix_expression(x);
				/**/
				if (x.entry.type instanceof Record)
					return add;
				Temp res = new Temp();
				/**/
				if(x.entry.type instanceof Char) {
					Inter.add(new Load_byte(res, add, 0));
				}
				else if (x.entry.type instanceof Array){   //
					Inter.add(new Move(res, add));
				}
				else {
					Inter.add(new Load_word(res, add, 0));
				}
				return res;
		}
	}
	
	private boolean Global(Expr expr) {
		if (!(expr instanceof Primary_expression)) return false;
		Primary_expression x = (Primary_expression) expr;
		if (x.op != Primary_expression.Op.ID) return false;
		VarEntry entry = (VarEntry) x.entry;
		if (entry.function == null) 
			return true;
		else
			return false;
	}
	
	private int Interpret_arguments(Arguments args)	 {//return the next offset to record the possible struct return address
		int paraSize = 0;
		int tmpparasize = 0;
		Temp tmp;
		LinkedList<Send_param> arg_list = new LinkedList<Send_param>();
		for (Expr x: args.exprs) {
			if (paraSize%4!=0&&!(x.entry.type instanceof Char)) 
				paraSize = (paraSize/4+1)*4;
			if (x.entry.type instanceof Record) {
			//	tmp = address_all_expression(x);
				tmp = Interpret_all_expression(x);
				Temp add = new Temp();
				Inter.add(new Binary_Imm(Double_expression.Op.PLUS, add, sp, paraSize));
				int size = Sizeof.sizeof(x.entry.type);
				paraSize = paraSize + size;
				arg_list.add(new Send_param(add, tmp, size));
				//memcpy(add, tmp, size);
			}
			else {
				if (x.entry.type instanceof Array) {   //may need to change/
					tmp = address_all_expression(x);
					if (!(Global(x)))
						Inter.add(new Load_word(tmp, tmp, 0));
				}
				else
					tmp = Interpret_all_expression(x);
				int size = Sizeof.sizeof(x.entry.type);
				if (x.entry.type instanceof Array)       //may need to change
					size = 4;
				arg_list.add(new Send_param(tmp, tmpparasize, size));   //paraSize
				paraSize = paraSize + size;
				if (size == 1)
					tmpparasize = tmpparasize + 4;
				else
					tmpparasize = tmpparasize + size;
			}
		}
		if (paraSize%4!=0) 
			paraSize = (paraSize/4+1)*4;
		for (Send_param x: arg_list) {
			if (x.reg != null)
				Inter.add(x);
			else {
					Temp tmp1 = new Temp();
					for (int i = 0; i < x.size; i = i + 4) {
						Inter.add(new Load_word(tmp1, x.tmp, i));
						Inter.add(new Store_word(tmp1, x.add, i));
					}
			}
		}
		if (paraSize > Inter.MaxParaSize)
			Inter.MaxParaSize = paraSize;
		return paraSize;
	}
	
	private Temp address_all_expression(Expr x) {//return a Temp indicating the address of Expr x
		//must be Primary_expression, Postfix_expression, Unary_expression, Assignment_expression for record copy
		if (x instanceof Primary_expression) 
			return address_primary_expression((Primary_expression) x);
		else if (x instanceof Unary_expression) 
			return address_unary_expression((Unary_expression) x); 
		else if (x instanceof Assignment_expression)
			return address_assignment_expression((Assignment_expression) x);
		else 
			return address_postfix_expression((Postfix_expression) x);
	}
	
	private Temp address_primary_expression(Primary_expression x) { //ID
		Temp add;
		VarEntry entry = (VarEntry) x.entry;
		if (entry.function == null && (!Flag)) {
		//if(entry.function==null) {	//global variable
		//	if(entry.addFunc!=Func_now)	{ //first setup of add in this function
				add = new Temp();
				Inter.add(new Load_addr(add, entry.label));
				entry.addFunc = Func_now;
				entry.add = add;
		//	}
		//	else
		//		add = entry.add;
			return add;
		}
		else {				//local variable
			if (!(entry.type instanceof Record) && !(entry.type instanceof Array)) { /**/
				add = entry.getLoc();
				Temp res = new Temp();
				Inter.add(new Address(res, add));
				return res; 
			}
			
			add = new Temp();
			if(entry.offset < entry.function.paraSize)	//parameter variables
				Inter.add(new Binary_Imm(Double_expression.Op.PLUS, add, fp, entry.offset));
			else	//local declared variables
				Inter.add(new Binary_Imm(Double_expression.Op.MINUS, add, fp, entry.function.varSize + Sizeof.init_func() - entry.offset));
		}
		
		if(!(entry.type instanceof Record) && !(entry.type instanceof Array) && entry.loc!=null) {	//regular variable need write through before read its content in memory
			add = new Temp();
			if(entry.type instanceof Char)
				Inter.add(new Store_byte(entry.loc, add, 0));
			else
				Inter.add(new Store_word(entry.loc, add, 0));
		} 
		return add;
		
	}
	private Temp address_unary_expression(Unary_expression x) {  //STAR
		Temp res = Interpret_all_expression(x.expr);
		return res;
	}
	
	private Temp address_assignment_expression(Assignment_expression x) {	//can only be called during record copy, return the address of the record
		Temp left_address = address_all_expression(x.left);
		if ((x.right instanceof Postfix_expression) && (((Postfix_expression) x.right).op==Postfix_expression.Op.PAREN)) {	//copy the record returned from a function
			Postfix_expression expr = (Postfix_expression) x.right;
			int paraSize = Interpret_arguments((Arguments) expr.obj);
			Inter.add(new Send_param(left_address, paraSize, Sizeof.sizeof(new Pointer())));	//directly write the result at address left_address
			Inter.add(new Use_func(expr.expr.toString()));
			return left_address;
		}
		else {
			Temp right_address = address_all_expression(x.right);
			int size = Sizeof.sizeof(x.left.entry.type);
			memcpy(left_address, right_address, size);	
		}
		return left_address;	//return the start address of this struct as x's return value
	}
	
	private boolean Argument_array(Expr expr) {   /**/
		if (!(expr instanceof Primary_expression)) return false;
		Primary_expression x = (Primary_expression) expr;
		if (x.op != Primary_expression.Op.ID) return false;
		VarEntry entry = (VarEntry) x.entry;
		if (entry.getLoc().number>Num_argument) return false;
		return true;
	}
	
	private Temp address_postfix_expression(Postfix_expression x) {
		switch(x.op) {
			case MPAREN:
				Type type = x.expr.entry.type;
				if (type instanceof Array) {
					Array array = (Array) type;
					Temp old_address = address_all_expression(x.expr);
					if (Argument_array(x.expr)) /*special*/
						Inter.add(new Load_word(old_address, old_address, 0));
					Temp new_address = new Temp();
					Expr tar = (Expr) x.obj;
					if(tar.isConst) {
						int tmp = tar.result * Sizeof.sizeof(array.elementType);
						Inter.add(new Binary_Imm(Double_expression.Op.PLUS, new_address, old_address, tmp));
					}
					else {
						Temp reg = Interpret_all_expression(tar);
						Temp res = new Temp();
						Inter.add(new Binary_Imm(Double_expression.Op.TIMES, res, reg, Sizeof.sizeof(array.elementType)));
						Inter.add(new Binary(Double_expression.Op.PLUS, new_address, old_address, res));
					}
					return new_address;
				}
				else {	//Pointer
					Pointer pointer = (Pointer) type;
					Temp old_address = Interpret_all_expression(x.expr);
					Temp new_address = new Temp();
					Expr tar = (Expr) x.obj;
					if(tar.isConst) {
						int tmp = tar.result * Sizeof.sizeof(pointer.elementType);
						Inter.add(new Binary_Imm(Double_expression.Op.PLUS, new_address, old_address, tmp));
					}
					else {
						Temp reg = Interpret_all_expression(tar);
						Temp res = new Temp();
						Inter.add(new Binary_Imm(Double_expression.Op.TIMES, res, reg, Sizeof.sizeof(pointer.elementType)));
						Inter.add(new Binary(Double_expression.Op.PLUS, new_address, old_address, res));
					}
					return new_address;
				}
			case PAREN:	//function return a record, but no variable to get it
				Temp add = new Temp();
				int size = Sizeof.sizeof(x.entry.type);
				Inter.add(new Binary_Imm(Double_expression.Op.MINUS, add, fp, Func_now.varSize + Func_now.tempSize - Func_now.paraSize + Sizeof.init_func() + (size/4+1)*4));
				Func_now.tempSize = Func_now.tempSize + (size / 4 + 1) * 4;
				int paraSize = Interpret_arguments((Arguments) x.obj);
				Inter.add(new Send_param(add, paraSize, Sizeof.sizeof(new Pointer())));	//directly write the result at address add
				Inter.add(new Use_func(x.expr.toString()));
				return add;
			case DOT:
				Record ty = (Record) x.expr.entry.type;
				Temp old_address = address_all_expression(x.expr);
				//Temp old_address = Interpret_all_expression(x.expr);
				Temp new_address = new Temp();	
				String name = (String) x.obj;
				VarEntry entry = (VarEntry) ty.env.get(name);
				if (ty instanceof Struct) {
					Inter.add(new Binary_Imm(Double_expression.Op.PLUS, new_address, old_address, entry.offset));
					return new_address;
				}
				else
					return old_address;
			default: //PTR
				Pointer tmp_type = (Pointer) x.expr.entry.type;
				ty = (Record) tmp_type.elementType;
				new_address = new Temp();
				old_address = Interpret_all_expression(x.expr);
				name = (String) x.obj;
				entry = (VarEntry) ty.env.get(name);
				Inter.add(new Binary_Imm(Double_expression.Op.PLUS, new_address, old_address, entry.offset));
				return new_address;
		}
	}

	private Temp address_identifier(VarEntry entry)	{	//local array initializer
		Temp add = new Temp();
		Inter.add(new Binary_Imm(Double_expression.Op.MINUS, add, fp, entry.function.varSize + Sizeof.init_func() - entry.offset));
		return add;
	}
	
	private void memset(Temp des, int size, int number)	{//set number from des to des+size-1
		Temp num;
		if(number!=0) num = new Temp();    /**/
		else num = ze;
		for(int i=0;i<size;i=i+4)
			Inter.add(new Store_word(num, des, i));
	}
	
	private void memcpy(Temp tar, Temp source, int size) { //copy from source to tar with size
		Temp tmp = new Temp();
		for (int i = 0; i < size; i = i + 4) {
			Inter.add(new Load_word(tmp, source, i));
			Inter.add(new Store_word(tmp, tar, i));
		}
	}
}

