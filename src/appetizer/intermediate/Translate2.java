package appetizer.intermediate;

import java.util.LinkedList;

import appetizer.ast.*;
import appetizer.functions.Printf;
import appetizer.functions.Printf2;
import appetizer.optimize.Optimize_mips;
import appetizer.translate.*;

public class Translate2 {
	
	public Intermediate Inter;
	public int base, Parasize, Total_temp;
	public LinkedList<MIPS> mips = new LinkedList<MIPS>();
	public Optimize_mips opt;
	public Temp t0 = new Temp(-8), t1 = new Temp(-9);
	public int p[] = new int[50];
	
	public Translate2(Intermediate inter) {
		Inter = inter;
		base = 0;
		Parasize = Inter.MaxParaSize;            //
		mips.clear();
	}
	
	public void translate() {
		System.out.println("\t.data");
		for(Document x: Inter.documents) translate(x);
		System.out.println("\t.text");
		System.out.println("\t.align 2");
		System.out.println("\t.globl main");
		for(Quad x: Inter.list) translate(x);
		opt = new Optimize_mips(mips);
		mips = opt.Optimize();
		for(MIPS x: mips) System.out.println(x);
		Printf print = new Printf();
		System.out.println(print.mips());
		Printf2 print2 = new Printf2();
		System.out.println(print2.mips());
	}
	
	private Temp get_reg_word(Temp t, int bak) {
		if (t.number>0) {
			mips.add(new LW(new Temp(bak), t, base));
			return (new Temp(bak));
		}
		else 
			return t;
	}

	private Temp get_reg_byte(Temp t, int bak) {
		if (t.number>0) {
			mips.add(new LB(new Temp(bak), t, base));
			return (new Temp(bak));
		}
		else 
			return t;
	}
	
	private void translate(Quad x) {
		Temp mark0, mark1, mark2;
		p[5] = -13;
		p[6] = -14;
		if (x instanceof Beqz) {
			Beqz t = (Beqz) x;
			mark0 = get_reg_word(t.reg, -8);
			mips.add(new Mips_beqz(mark0, t.label));
		}
		else if (x instanceof Binary) {
			Binary t = (Binary) x;
			mark1 = get_reg_word(t.left, -8);
			mark2 = get_reg_word(t.right, -9);
			if (t.res.number>0) {
				mips.add(new Mips_binary(t.oper(), t0, mark1, mark2));   
				mips.add(new SW(t0, t.res, base));
			}
			else
				mips.add(new Mips_binary(t.oper(), get_reg_word(t.res, 0), mark1, mark2));
		}
		else if (x instanceof Binary_Imm) {
			Binary_Imm t = (Binary_Imm) x;
			mark1 = get_reg_word(t.left, -8);
			if (t.res.number>0) {
				mips.add(new Mips_binary(t.oper(), t0, mark1, t.right));   
				mips.add(new SW(t0, t.res, base));
			}
			else
				mips.add(new Mips_binary(t.oper(), get_reg_word(t.res, 0), mark1, t.right));
		}
		else if (x instanceof Bnez) {
			Bnez t = (Bnez) x;
			mark0 = get_reg_word(t.reg, -8);
			mips.add(new Mips_bnez(mark0, t.label));
		}
		else if (x instanceof Comp) {
			Comp t = (Comp) x;
			mark0 = get_reg_word(t.reg1, -8);
			mark1 = get_reg_word(t.reg2, -9);
			if (t.res.number>0) {
				mips.add(new Mips_comp(t.oper(), t0, mark0, mark1));   
				mips.add(new SW(t0, t.res, base));
			}
			else
				mips.add(new Mips_comp(t.oper(), get_reg_word(t.res, 0), mark0, mark1));
			//mips.add(new Sentence("\t"+t.oper()+" $t0, $t0, $t1"));
		}
		else if (x instanceof Comp_ch) {
			Comp_ch t = (Comp_ch) x;
			mark0 = get_reg_byte(t.reg1, -8);
			mark1 = get_reg_byte(t.reg2, -9);
			if (t.res.number>0) {
				mips.add(new Mips_comp(t.oper(), t0, mark0, mark1));   
				mips.add(new SW(t0, t.res, base));
			}
			else
				mips.add(new Mips_comp(t.oper(), get_reg_word(t.res, -8), mark0, mark1));
			//mips.add(new Sentence("\t"+t.oper()+" $t0, $t0, $t1"));
		}
		else if (x instanceof Use_func) {
			Use_func t = (Use_func) x;
			mips.add(new Sentence("\tjal " + "Function_" + t.name));
			if (t.res != null) {
				if (t.res.number>0)
					mips.add(new SW(new Temp(-2), t.res, base));
				else
					mips.add(new Mips_move(get_reg_word(t.res, 0), new Temp(-2)));
			}
		}
		else if (x instanceof Malloc) {
			Malloc t = (Malloc) x;
			if (t.tmp.number>0)
				mips.add(new LW(new Temp(-4), t.tmp, base));
			else
				mips.add(new Mips_move(new Temp(-4), get_reg_word(t.tmp, 0)));
			mips.add(new Sentence("\tli, $v0, 9"));
			mips.add(new Sentence("\tsyscall"));

			if (t.add.number>0)
				mips.add(new SW(new Temp(-2), t.add, base));
			else
				mips.add(new Mips_move(get_reg_word(t.add, 0), new Temp(-2)));
		}
		else if (x instanceof Jump) {
			Jump t = (Jump) x;
			mips.add(new Sentence(t.Mips()));
		}
		else if (x instanceof Label_quad) {
			Label_quad t = (Label_quad) x;
			if (t.label.func==null)
				mips.add(new Sentence(t.Mips()));
			else {
				base = 8 + t.label.func.varSize;
				Total_temp = t.label.func.tempSize;
				mips.add(new Sentence(t.Mips()));
				mips.add(new Sentence("\tsw $fp, -4($sp)"));
				mips.add(new Sentence("\tsw $ra, -8($sp)"));
				mips.add(new Sentence("\tmove $fp, $sp"));
				mips.add(new Sentence("\tsub $sp, $sp, " + (base + t.label.func.paraSize + Total_temp + Parasize + 4 )));
			}
		}
		else if (x instanceof Load_addr) {
			Load_addr t = (Load_addr) x;
			if (t.reg.number>0) {
				mips.add(new LA(t0, t.label));
				mips.add(new SW(t0, t.reg, base));
			}
			else {
				mips.add(new LA(get_reg_word(t.reg, 0), t.label));
			}
		}
		else if (x instanceof Load_word) {      //
			Load_word t = (Load_word) x;
			mark0 = get_reg_word(t.add, -10);
			if (t.res.number>0) {
				mips.add(new Sentence("\tlw $t0, " + t.offset + "(" + mark0 + ")"));   /**/
				mips.add(new SW(t0, t.res, base));
			}
			else {
				mark1 = get_reg_word(t.res, 0);
				mips.add(new Sentence("\tlw " + mark1 + ", " + t.offset + "(" + mark0 + ")"));  /**/
			}
		}
		else if (x instanceof Load_byte) {       //
			Load_byte t = (Load_byte) x;
			mark0 = get_reg_word(t.add, -10);
			if (t.res.number>0) {
				mips.add(new Sentence("\tlb $t0, " + t.offset + "(" + mark0 + ")"));   /**/
				mips.add(new SB(t0, t.res, base));
			}
			else {
				mark1 = get_reg_word(t.res, 0);
				mips.add(new Sentence("\tlb " + mark1 + ", " + t.offset + "(" + mark0 + ")"));  /**/
			}
		}
		else if (x instanceof Load_imm) {
			Load_imm t = (Load_imm) x;
			if (t.res.number>0) {
				mips.add(new Sentence("\tli $t0, "+t.number));   /**/
				mips.add(new SW(t0, t.res, base));
			}
			else {
				mark0 = get_reg_word(t.res, 0);
				mips.add(new LI(mark0, t.number));
			}
		}
		else if (x instanceof LoadB_imm) {
			LoadB_imm t = (LoadB_imm) x;
			if (t.res.number>0) {
				mips.add(new Sentence("\tli $t0, "+t.number));
				mips.add(new SB(new Temp(-8), t.res, base));
			}
			else {
				mark0 = get_reg_word(t.res, 0);
				mips.add(new LI(mark0, t.number));
			}
		}
		else if (x instanceof Move) {
			Move t = (Move) x;
			mark0 = get_reg_word(t.ori, -8);
			if (t.res.number>0) 
				mips.add(new SW(mark0, t.res, base));
			else {
				mark1 = get_reg_word(t.res, 0);
				mips.add(new Mips_move(mark1, mark0));
			}
		}
		else if (x instanceof Send_param) {
			Send_param t = (Send_param) x;
			if (t.size == 1) {
				mark0 = get_reg_byte(t.reg, -8);
				mips.add(new Sentence("\tsb " + mark0 + ", " + (t.start) + "($sp)"));  /**/
			}
			else {
				mark0 = get_reg_word(t.reg, -8);
				mips.add(new Sentence("\tsw " + mark0 + ", " + (t.start) + "($sp)"));  /**/
			}
		}
		else if (x instanceof Return) {
			Return t = (Return) x;
			if (t.reg !=null) {
				if (t.reg.number>0)
					mips.add(new LW(new Temp(-2), t.reg, base));
				else {
					mark0 = get_reg_word(t.reg, 0);
					mips.add(new Mips_move(new Temp(-2), mark0));
				}
			}
			mips.add(new Sentence("\tmove $sp, $fp"));
			mips.add(new Sentence("\tlw $fp, -4($sp)"));
			mips.add(new Sentence("\tlw $ra, -8($sp)"));
			mips.add(new Sentence("\tjr $ra"));
		}
		else if (x instanceof Store_byte) {     
			Store_byte t = (Store_byte) x;
			mark0 = get_reg_word(t.add, -10);
			mark1 = get_reg_byte(t.reg, -8);
			mips.add(new Sentence("\tsb" + mark1 + ", " + t.offset + "(" + mark0 + ")"));   /**/
		}
		else if (x instanceof Store_word) {
			Store_word t = (Store_word) x;
			mark0 = get_reg_word(t.add, -10);
			mark1 = get_reg_word(t.reg, -8);
			mips.add(new Sentence("\tsw " + mark1 + ", " + t.offset + "(" + mark0 + ")"));   /**/
		}
		else if (x instanceof End_func) {
			mips.add(new Sentence("\tmove $sp, $fp"));
			mips.add(new Sentence("\tlw $fp, -4($sp)"));
			mips.add(new Sentence("\tlw $ra, -8($sp)"));
			mips.add(new Sentence("\tjr $ra"));
		}
		else if (x instanceof Receive_param) {
			Receive_param t = (Receive_param) x;
			if (t.size == 1) {
				if (t.res.number>0) {
					mips.add(new Sentence("\tlb $t0, " + (t.start) + "($fp)"));   /**/
					mips.add(new SB(t0, t.res, base));
				}
				else {
					mark0 = get_reg_word(t.res, 0);
					mips.add(new Sentence("\tlb " + mark0 + ", " + (t.start) + "($fp)"));    /**/
				}
			}
			else {
				if (t.res.number>0) {
					mips.add(new Sentence("\tlw $t0, " + (t.start) + "($fp)"));    /**/
					mips.add(new SW(t0, t.res, base));
				}
				else {
					mark0 = get_reg_word(t.res, 0);
					mips.add(new Sentence("\tlw " + mark0 + ", " + (t.start) + "($fp)"));    /**/
				}
			}
		}
		else if (x instanceof Unary) {
			Unary t = (Unary) x;
			mark0 = get_reg_word(t.reg, -8);
			if (t.res.number>0) {
				mips.add(new Mips_unary(t.oper(), t0, mark0));
				mips.add(new SW(t0, t.res, base));
			}
			else {
				mark1 = get_reg_word(t.res, 0);
				mips.add(new Mips_unary(t.oper(), mark1, mark0));
			}
		}
		else if (x instanceof Address) { //
			Address t = (Address) x;
			if (t.res.number>0) {
				mips.add(new LW(new Temp(-10), new Temp(-30), 0));
				mips.add(new Sentence("\tsub $t2, $t2, " + t.ori.offset(base)));
				mips.add(new SW(new Temp(-10), t.res, base));
			}
			else {
				mark0 = get_reg_word(t.res, 0);
				mark1 = new Temp(-30);
				mips.add(new Sentence("\tsub " + mark0 + ", " + mark1 + ", " + t.ori.offset(base)));
			}
		}
		else if (x instanceof Load_w) {
			Load_w t = (Load_w) x;
			mips.add(new LW(t.t1, t.t2, base));
		}
		else if (x instanceof Store_w) {
			Store_w t = (Store_w) x;
			mips.add(new SW(t.t1, t.t2, base));
		}
		else if (x instanceof Gap) {
			
		}
		else {
			mips.add(new Sentence("WHAT?"));
		}
	}

	private void translate(Document x) {
		if (x instanceof _Label)
			System.out.println(x);
		else
			System.out.println("\t"+x);
	}
	

}
