package appetizer.intermediate;

public class Space implements Document {
	public int size;

	public Space(int size)
	{
		this.size = size;
	}
	public String toString()
	{
		return ".space "+size;
	}
}
