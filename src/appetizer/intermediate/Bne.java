package appetizer.intermediate;

public class Bne implements Quad 
{
	public Temp reg1, reg2;
	public Label label;

	public Bne(Temp r, Temp r2, Label l)
	{
		reg1 = r;
		reg2 = r2;
		label = l;
	}
	public String toString()
	{
		return "bne "+reg1+", "+reg2+" goto "+label;
	}
}
