package appetizer.intermediate;

public class Move implements Quad
{
	public Temp res, ori;

	public Move(Temp r, Temp o)
	{
		res = r;
		ori = o;
	}
	public String toString()
	{
		return res+" = "+ori;
	}
}
