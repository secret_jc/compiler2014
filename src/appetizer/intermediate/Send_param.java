package appetizer.intermediate;

public class Send_param implements Quad
{
	public Temp reg, add, tmp;
	public int size, start;
	
	public Send_param(Temp a, Temp t, int s) {
		add = a;
		tmp = t;
		size = s;
		reg = null;
	}
	
	public Send_param(Temp r, int s, int si)
	{
		reg = r;
		start = s;
		size = si;
	}
	public String toString()
	{	
		if (reg != null)
			return "Send_param "+reg+" "+start+", "+size;
		else
			return "Send_param2 " + add + tmp + size;
	}
}
