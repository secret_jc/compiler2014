package appetizer.intermediate;

public class Return implements Quad
{
	public Temp reg;

	public Return(Temp r)
	{
		reg = r;
	}
	public String toString()
	{
		return "return "+reg;
	}
}
