package appetizer.intermediate;

public class Receive_param implements Quad {
	
	public Temp res;
	public int start;
	public int size;
	
	public Receive_param(Temp r, int offset, int sizeof) {
		res = r;
		start = offset;
		size = sizeof;
	}
	
	public String toString()
	{
		return "Reiceive_param "+res+" "+start+", "+size;
	}
}
