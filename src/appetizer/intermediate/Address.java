package appetizer.intermediate;

public class Address implements Quad {
	
	public Temp res, ori;

	public Address(Temp r, Temp a) {
		res = r;
		ori = a;
	}
	
	public String toString() {
		return res + " = " + "Address of " + ori;
	}

}
