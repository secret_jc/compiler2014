package appetizer.intermediate;

public class Store_byte implements Quad
{
	public Temp reg, add;
	public int offset;

	public Store_byte(Temp r, Temp a, int o)
	{
		reg = r;
		add = a;
		offset = o;
	}
	public String toString()
	{
		return "sb "+reg+", "+offset+"("+add+")";
	}
}
