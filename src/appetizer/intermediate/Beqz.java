package appetizer.intermediate;

public class Beqz implements Quad 
{
	public Temp reg;
	public Label label;

	public Beqz(Temp r, Label l)
	{
		reg = r;
		label = l;
	}
	public String toString()
	{
		return "beqz "+reg+" goto "+label;
	}
	public String Mips(int base) {
		return "\tbeqz $t0, "+label;
	}
}
