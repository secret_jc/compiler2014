package appetizer.intermediate;

public class Jal implements Quad 
{
	public Label label;

	public Jal(Label l)
	{
		label = l;
	}
	public String toString()
	{
		return "jal "+label;
	}
}
