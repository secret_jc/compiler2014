package appetizer.syntactic;
import java.io.*;

%%

%class Lexer
%cup
%unicode
%line
%column
%public
%implements Symbols

%{
	private char CharContent;
	private StringBuffer StringContent;
	private int CharLength;

	private void error(String message)
	{
		System.err.println("Lexer detects an error in line "+yyline+" column "+yycolumn+": "+message);
		System.exit(1);    //exit with error report
	}
	private java_cup.runtime.Symbol token(int kind)
	{
		return new java_cup.runtime.Symbol(kind, yyline, yycolumn);
	}
	private java_cup.runtime.Symbol token(int kind, Object value)
	{
		return new java_cup.runtime.Symbol(kind, yyline, yycolumn, value);
	}

	private Integer charToInt(int flag, String x)
	{
		if(flag==1)   //OctInt
		{
			int res=0;
			for(char i:x.toCharArray())
			{
				if(i>='0'&&i<='7') res=res*8+i-'0';
			}
			return new Integer(res);
		}

		//HexInt
		int res=0;
		for(char i:x.toCharArray())
		{
			if(i>='0'&&i<='9') res=res*16+i-'0';
			else if(i>='a'&&i<='f') res=res*16+i-'a'+10;
			else res=res*16+i-'A'+10;
		}
		return new Integer(res);
	}
%}

%eofval{
	if(yystate() == YYCOMMENT)
	{
		error("Comment symbol do not match until the end of file!");
	}
	if(yystate() == YYSTRING)
	{
		error("\" symbol do not match until the end of file!");
	}
	if(yystate() == YYCHAR)
	{
		error("\' symbol do not match until the end of file!");
	}
	return token(EOF, null);  //return Object "null"
%eofval}

LineTerm = \n|\r|\r\n
Letter = [_$a-zA-Z]
Digit = [0-9]
White = [ \t\f]
WhiteSpace = {LineTerm}|{White}

Identifier = {Letter} ({Letter}|{Digit})*
DecInteger = ([1-9] {Digit}*)|0
OctInteger = "0" [0-7]+
HexInteger = ("0X"|"0x") ({Digit}|[a-fA-F])+

CharOctInteger = \'\\0[0-7][0-7]?\'
CharHexInteger = \'\\x[0-9a-fA-F][0-9a-fA-F]?\'

IncludeComment = ("#" {White}* "include" {White}* "<" [a-zA-Z.]* ">")|("#" {White}* "include" {White}* "\"" [a-zA-Z.]* "\"")

%state YYCOMMENT, YYCOMMENTLINE, YYSTRING, YYCHAR

%%

<YYINITIAL>
{
	/* comment */

	"/*" { yybegin(YYCOMMENT); }
	"*/" { error("Comment does not match!"); }
	"//" { yybegin(YYCOMMENTLINE); }
	{IncludeComment} {/*ignore all include contents.*/}
	
	/* keyword */

	"void"     {return token(VOID);}
	"char"     {return token(CHAR);}
	"int"      {return token(INT);}
	"struct"   {return token(STRUCT);}
	"union"    {return token(UNION);}
	"if"       {return token(IF);}
	"else"     {return token(ELSE);}
	"while"    {return token(WHILE);}
	"for"      {return token(FOR);}
	"continue" {return token(CONTINUE);}
	"break"    {return token(BREAK);}
	"return"   {return token(RETURN);}
	"sizeof"   {return token(SIZEOF);}

	{CharOctInteger} {return token(NUM, charToInt(1, yytext()));}
	{CharHexInteger} {return token(NUM, charToInt(2, yytext()));}

	/* charactor and string */

	"\'" {
		yybegin(YYCHAR);
		CharLength = 0;
	}
	"\"" {
		yybegin(YYSTRING);
		StringContent = new StringBuffer();
	}

	/* operator */

	"(" { return token(LPAREN); }
	")" { return token(RPAREN); }
	"{" { return token(LBRACE); }
	"}" { return token(RBRACE); }
	"[" { return token(LMPAREN); }
	"]" { return token(RMPAREN); }

	";" { return token(SEMI); }
	"," { return token(COMMA); }

	"+" { return token(PLUS); }
	"-" { return token(MINUS); }
	"*" { return token(TIMES); }
	"/" { return token(DIVIDE); }
	"%" { return token(MODULE); }
	
	"++" { return token(INC); }
	"--" { return token(DEC); }

	"*="  { return token(MUL_ASSIGN); }
	"/="  { return token(DIV_ASSIGN); }
	"%="  { return token(MOD_ASSIGN); }
	"+="  { return token(ADD_ASSIGN); }
	"-="  { return token(SUB_ASSIGN); }
	"<<=" { return token(SHL_ASSIGN); }
	">>=" { return token(SHR_ASSIGN); }
	"&="  { return token(AND_ASSIGN); }
	"^="  { return token(XOR_ASSIGN); }
	"|="  { return token(OR_ASSIGN); }

	
	"!"   { return token(NOT); }
	"||"  { return token(OR); }
	"&&"  { return token(AND); }

	"."  { return token(DOT); }
	"->" { return token(PTR); }

	"==" { return token(EQ); }
	"!=" { return token(NE); }
	"<"  { return token(LT); }
	"<=" { return token(LE); }
	">"  { return token(GT); }
	">=" { return token(GE); }

	"="  { return token(ASSIGN);}

	"|"  { return token(BIT_OR); }
	"^"  { return token(BIT_XOR); }
	"&"  { return token(BIT_AND); }
	"~"  { return token(BIT_NOT); }
	"<<" { return token(SHL); }
	">>" { return token(SHR); }


	{Identifier} {return token(ID, yytext());}
	{DecInteger} {return token(NUM, new Integer(yytext()));}
	{OctInteger} {return token(NUM, Integer.valueOf(yytext().substring(1), 8));}
	{HexInteger} {return token(NUM, Integer.valueOf(yytext().substring(2),16));}

	{WhiteSpace} {/* ignore whitespace */}
	
	[^] { error("Illegal character "+yytext()+" in line "+yyline+" column "+yycolumn); }

}


<YYCOMMENT>
{
	"*/" {yybegin(YYINITIAL);}
	[^] {/* ignore all the comment */}
}

<YYCOMMENTLINE>
{
	{LineTerm} {yybegin(YYINITIAL);}
	[^] {/* ignore all the comment */}
}

<YYCHAR>
{
	"\'" {
		if(CharLength == 1)
		{
			yybegin(YYINITIAL);
			return token(CHARCONTENT, new Character(CharContent));
		}
		else error("Not a charactor!");
	}
	"\\t"  {CharContent = '\t'; ++CharLength;}
	"\\n"  {CharContent = '\n'; ++CharLength;}
	"\\r"  {CharContent = '\r'; ++CharLength;}
	"\\"   {CharContent = '\\'; ++CharLength;}
	"\\\'" {CharContent = '\''; ++CharLength;}
	"\\\"" {CharContent = '\"'; ++CharLength;}
	{LineTerm} 	{error("Not a charactor! An invalid character detected!");}
	[^]    {CharContent = yytext().charAt(0); ++CharLength;}
}

<YYSTRING>
{	
	"\"" {
		yybegin(YYINITIAL);
		return token(STRINGCONTENT, StringContent.toString());
	}
	"\\t"  {StringContent.append('\t');}
	"\\n"  {StringContent.append('\n');}
	"\\r"  {StringContent.append('\r');}
	"\\"   {StringContent.append('\\');}
	"\\\'" {StringContent.append('\'');}
	"\\\"" {StringContent.append('\"');}
	{LineTerm} 	{error("Not a string! An invalid character detected!");}
	[^]    {StringContent.append(yytext().charAt(0));}
}

