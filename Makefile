.PHONY: all
all:
	mkdir -p bin
	javac -encoding gbk src/appetizer/*/*.java -classpath src/appetizer/syntactic/java-cup-11a-runtime.jar:src/appetizer/syntactic/JFlex.jar:src/appetizer/syntactic/java-cup-11a.jar -d bin
.PHONY: clean
clean:
	rm -rf bin
